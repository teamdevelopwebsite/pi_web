@extends('Backend.main')

@section('head')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('/Backend/dist/css/dataTables.bootstrap.min.css')}}">


@stop

@section('content')
    <div class="container" style="background: white">
        <div class="card">
            <h3 class="card-title">Add new quizzes</h3>
            <div class="card-body">
                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Quizzes Tittle</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="" placeholder="Title">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <style>


                                    .image--cover {
                                        width: 40px;
                                        height: 40px;
                                        border-radius: 50%;
                                        object-fit: cover;
                                        object-position: center right;
                                        margin-left: 0px;
                                    }

                                </style>
                                <img src="https://i.kinja-img.com/gawker-media/image/upload/gd8ljenaeahpn0wslmlz.jpg"
                                     class="image--cover">


                                Posting as Kimhak


                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="submit" class="btn btn-primary">Revert to Traft</button>
                                <button type="submit" class="btn btn-primary">Close</button>
                            </div>
                        </div>
                        <hr>


                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="box box-info">
                                        <form class="form-horizontal">
                                            <div class="box-body" style="height: 730px">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-2 ">Youtube Src</label>
                                                    <div class="col-sm-8">
                                                        <input type="email" class="form-control" id="">
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <a class="btn btn-block btn btn-primary">
                                                            <i class=""></i> Add Src
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-2 ">Thumbnail</label>
                                                    <div class="col-sm-8">
                                                        <input type="file" name="pic" accept="image/*">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="inputEmail3"
                                                           class="col-sm-2">Description</label>
                                                    <div id="sample">
                                                        <script type="text/javascript"
                                                                src="http://js.nicedit.com/nicEdit-latest.js"></script>
                                                        <script type="text/javascript">
                                                            bkLib.onDomLoaded(function () {
                                                                nicEditors.allTextAreas()
                                                            });
                                                        </script>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <textarea name="area2" style="width: 100%; height: 450px">
                                                            please input the text
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <br>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="box box-default">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Category</h3>

                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                        class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-footer" style="">
                                            <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#tab_1" data-toggle="tab"
                                                                          aria-expanded="true">All Category</a></li>
                                                    <li class=""><a href="#tab_2" data-toggle="tab"
                                                                    aria-expanded="false">Create</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input type="checkbox">
                                                                    Checkbox 1
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input type="checkbox">
                                                                    Checkbox 2
                                                                </label>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="tab-pane" id="tab_2">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input type="checkbox">
                                                                    Checkbox 2
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box box-default">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Tags</h3>

                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                        class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-footer" style="">
                                            <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#tab_3" data-toggle="tab"
                                                                          aria-expanded="true">Add Tags</a></li>
                                                    <li class=""><a href="#tab_4" data-toggle="tab"
                                                                    aria-expanded="false">Most Used</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_3">
                                                        Tap 1
                                                    </div>

                                                    <div class="tab-pane" id="tab_4">
                                                        Tap 2
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box box-default">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Thumbnail</h3>

                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                        class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-footer" style="">
                                            <div class="nav-tabs-custom">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1">
                                                        <a href="#">Select Thumbnail</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box box-default">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Published On</h3>

                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                        class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-footer" style="">
                                            <div class="nav-tabs-custom">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_3">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input type="checkbox">
                                                                    Automatic
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input type="checkbox">
                                                                    Set Date and Time
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-8">

                                                                <div class="input-group date">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" class="form-control pull-right" id="datepicker">
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-4">


                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@section('script')
    <script>
        function openCity(cityName) {
            var i;
            var x = document.getElementsByClassName("city");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById(cityName).style.display = "block";
        }
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })


    </script>


@stop
@stop
