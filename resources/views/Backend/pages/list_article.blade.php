@extends('Backend.main')

@section('head')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('/Backend/dist/css/dataTables.bootstrap.min.css')}}">

    @section('style')
        <style type="text/css">
            .btn{
                padding: 2px 12px !important;
            }
        </style>
    @stop

@stop

@section('content')
    <div class="container" style="background: white">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title" style="text-align: center">All Article</h3>
            </div>

            <!-- /.card-header -->
            <div class="card-body">
                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-md-12">
                            <button id="btn-public" type="button" class="btn btn-warning">Public</button>
                            <button id="btn-draft" type="button" class="btn btn-warning">Move to draft</button>
                            <button id="btn-trash" type="button" class="btn btn-warning"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    <div class="row">
                        <hr style="margin-top: 10px;margin-bottom: 10px">
                        <div class="col-sm-12">
                            <table id="example1" class="table table-bordered table-striped dataTable" role="grid"
                                   aria-describedby="example1_info">
                                <thead>
                                <tr role="row">
                                    <th rowspan="1" colspan="1"><input name="select_all" value="1" type="checkbox"
                                                                       id="checkAll"></th>
                                    <th rowspan="1" colspan="1">Title Name</th>
                                    <th rowspan="1" colspan="1">Posted by</th>
                                    <th rowspan="1" colspan="1">Comments</th>
                                    <th rowspan="1" colspan="1">Views</th>
                                    <th rowspan="1" colspan="1">Posted At</th>
                                    <th rowspan="1" colspan="1">Status</th>
                                    <th rowspan="1" colspan="1">Actions</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="odd">
                                    <td><input type="checkbox" id="checkItem" class="checkbox"></td>
                                    <td>How to go home</td>
                                    <td>Kimhak</td>
                                    <td>100</td>
                                    <td>100</td>
                                    <td>10/10/2018</td>
                                    <td>Draft</td>
                                    <td style="text-align: center;padding-left: 0;padding-right: 0">
                                        <a href="#"><i class="fa fa-pencil" style="color:yellowgreen;"></i></a>
                                        <span>|</span>
                                        <a href="#"><i class="fa fa-trash" style="color:red;"></i></a>
                                        <span>|</span>
                                        <a href="#"><span>Public</span></a>
                                        <span>|</span>
                                        <a href="#"><span>Draft</span></a>
                                    </td>
                                </tr>
                                <tr role="row" class="odd">
                                    <td><input type="checkbox" id="checkItem" class="checkbox"></td>
                                    <td>How to go home</td>
                                    <td>Kimhak</td>
                                    <td>100</td>
                                    <td>100</td>
                                    <td>10/10/2018</td>
                                    <td>Draft</td>
                                    <td style="text-align: center;padding-left: 0;padding-right: 0">
                                        <a href="#"><i class="fa fa-pencil" style="color:yellowgreen;"></i></a>
                                        <span>|</span>
                                        <a href="#"><i class="fa fa-trash" style="color:red;"></i></a>
                                        <span>|</span>
                                        <a href="#"><span>Public</span></a>
                                        <span>|</span>
                                        <a href="#"><span>Draft</span></a>
                                    </td>
                                </tr>


                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th rowspan="1" colspan="1">Title Name</th>
                                    <th rowspan="1" colspan="1">Posted by</th>
                                    <th rowspan="1" colspan="1">Comments</th>
                                    <th rowspan="1" colspan="1">Views</th>
                                    <th rowspan="1" colspan="1">Posted At</th>
                                    <th rowspan="1" colspan="1">Status</th>
                                    <th rowspan="1" colspan="1">Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>

@section('script')

    <!-- DataTables -->
    <script src="{{asset('/Backend/dist/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/Backend/dist/js/dataTables.bootstrap.min.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            // $('#example1').DataTable()
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
                'sort': true
            })
        });
        $('#checkAll').click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });


    </script>
@stop
@stop
