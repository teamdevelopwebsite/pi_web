@extends('Backend.main')

@section('head')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('/Backend/dist/css/dataTables.bootstrap.min.css')}}">

@stop

@section('content')
    <div class="container" style="background: white">
        <div class="card">
            <h3 class="card-title">All Admins</h3>

            <!-- /.card-header -->
            <div class="card-body">
                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example1" class="table table-bordered table-striped dataTable" role="grid"
                                   aria-describedby="example1_info">
                                <thead>
                                <tr role="row">
                                    <th rowspan="1" colspan="1">Image</th>
                                    <th rowspan="1" colspan="1">User Name</th>
                                    <th rowspan="1" colspan="1">Date of Birth</th>
                                    <th rowspan="1" colspan="1">Email</th>
                                    <th rowspan="1" colspan="1">Gender</th>
                                    <th rowspan="1" colspan="1">Post</th>
                                    <th rowspan="1" colspan="1">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <td class="center"><img src="{{url('storage/images/lion.png')}}" alt=""
                                                        style="max-width: 50px;max-height: 50px"></td>
                                <td>Bunmeng Heng</td>
                                <td>23 August 1998</td>
                                <td>bunmengheng58@gmail.com</td>
                                <td>M</td>
                                <td>post</td>
                                <td style="text-align: center;padding-left: 0;padding-right: 0">
                                    <a href="#"><i class="fa fa-pencil" style="color:yellowgreen;"></i></a>
                                    <span>|</span>
                                    <a href="#"><i class="fa fa-trash" style="color:red;"></i></a>
                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@section('script')

    <!-- DataTables -->
    <script src="{{asset('/Backend/dist/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/Backend/dist/js/dataTables.bootstrap.min.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@stop
@stop
