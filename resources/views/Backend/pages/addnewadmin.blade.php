f@extends('Backend.main')

@section('head')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('/Backend/dist/css/dataTables.bootstrap.min.css')}}">


    <style>

        body {
            background-color: #efefef;
        }

        .profile-pic {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }

        .file-upload {
            display: none;
        }
        .circle {
            border-radius: 1000px !important;
            overflow: hidden;
            width: 128px;
            height: 128px;
            border: 8px solid rgba(255, 255, 255, 0.7);
            /*position: absolute;*/
            top: 72px;
        }
        img {
            max-width: 100%;
            height: auto;
        }
        .p-image {
            top: 167px;
            right: 30px;
            color: #666666;
            transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        }
        .p-image:hover {
            transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        }
        .upload-button {
            font-size: 1.2em;
        }

        .upload-button:hover {
            transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
            color: #999;
        }
    </style>


@stop

@section('content')
    <div class="container" style="background: white">
        <div class="card">
            <h3 class="card-title">Create a brand new user and add them to this site.</h3>





            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <form class="form-horizontal">

<Center>
                            <div class="small-12 medium-2 large-2 columns center-block">
                                <div class="circle">
                                    <!-- User Profile Image -->
                                    {{--<img class="profile-pic" src="http://cdn.cutestpaw.com/wp-content/uploads/2012/07/l-Wittle-puppy-yawning.jpg">--}}
                                    <input class="file-upload" type="file" accept="image/*"/>

                                </div>
                                <div class="p-image">
                                    <i class="fa fa-camera upload-button "></i>
                                    {{--<input class="file-upload" type="file" accept="image/*"/>--}}
                                </div>
                            </div>

</Center>
                            <div class="box-body">

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Username (required)</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" placeholder="Username">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label  class="col-sm-2 control-label">Email (required)</label>

                                    <div class="col-sm-10">
                                        <input type="email" class="form-control"  placeholder="Email">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Password</label>

                                    <div class="col-sm-10">
                                        <input type="password" class="form-control"
                                               placeholder="Password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label  class="col-sm-2 control-label">Confirm
                                        (required)</label>

                                    <div class="col-sm-10">
                                        <input type="password" class="form-control"
                                               placeholder="Confirm Password">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Birthday</label>

                                    <div class="col-sm-10">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <select class="form-control select2 select2-hidden-accessible"
                                                        style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                    <option selected="selected">Month</option>
                                                    <option></option>
                                                    <option></option>
                                                </select>
                                            </div>

                                            <div class="col-sm-4">
                                                <select class="form-control select2 select2-hidden-accessible"
                                                        style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                    <option selected="selected">Day</option>
                                                    <option></option>
                                                    <option></option>
                                                </select>
                                            </div>

                                            <div class="col-sm-4">
                                                <select class="form-control select2 select2-hidden-accessible"
                                                        style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                    <option selected="selected">Year</option>
                                                    <option></option>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Gender</label>

                                    <div class="col-sm-10">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <select class="form-control select2 select2-hidden-accessible"
                                                        style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>




                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Add New Admin</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>


                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>


    </div>
@section('script')


    <script>
        $(document).ready(function() {


            var readURL = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.profile-pic').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }


            $(".file-upload").on('change', function(){
                readURL(this);
            });

            $(".upload-button").on('click', function() {
                $(".file-upload").click();
            });
        });
    </script>

@stop
@stop
