<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('Backend/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>

                </a>

            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Article</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/admin/list-article')}}"><i class="fa fa-circle-o"></i> All (5) </a></li>
                    <li><a href="{{url('/admin/draftarticle')}}"><i class="fa fa-circle-o"></i> Draft </a></li>
                    <li><a href="{{url('/admin/addnewarticle')}}"><i class="fa fa-circle-o"></i> Add New Article</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-question"></i>
                    <span>Quizzes</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/admin/all-quizzes')}}"><i class="fa fa-circle-o"></i> All (2) </a></li>
                    <li><a href="{{url('/admin/draft-quiz')}}"><i class="fa fa-circle-o"></i> Draft </a></li>
                    <li><a href="{{url('/admin/add-new-quiz')}}"><i class="fa fa-circle-o"></i> Add New Quiz </a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-video-camera"></i>
                    <span>Video</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/admin/allvideos')}}"><i class="fa fa-circle-o"></i> All (2) </a></li>
                    <li><a href="{{url('/admin/draftvideo')}}"><i class="fa fa-circle-o"></i> Draft </a></li>
                    <li><a href="{{url('/admin/addnewvideo')}}"><i class="fa fa-circle-o"></i> Add New Videos </a></li>
                </ul>
            </li>
            <li>
                <a href="{{url('/admin/all-user')}}">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-secret"></i>
                    <span>Admin</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/admin/admin')}}"><i class="fa fa-circle-o"></i> All (2) </a></li>
                    <li><a href="{{url('/admin/add-new-admin')}}"><i class="fa fa-circle-o"></i> Add New Admin </a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
