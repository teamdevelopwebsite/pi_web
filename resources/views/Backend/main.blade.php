<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('Backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('Backend/bower_components/font-awesome/css/font-awesome.min.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('Backend/dist/css/AdminLTE.min.css')}}">

    <link rel="stylesheet" href="{{asset('Backend/dist/css/skins/_all-skins.min.css')}}">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @yield('head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

{{-- Header --}}
@include('Backend.partail.header')

<!-- Left side column. contains the logo and sidebar -->
@include('Backend.partail.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content">
            @yield('content')
        </section>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">

        <strong>Copyright &copy; 2014-2016 </strong> All rights
        reserved.
    </footer>


    <!-- jQuery 3 -->
    <script src="{{asset('Backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('Backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- AdminLTE App -->
    <script src="{{asset('Backend/dist/js/adminlte.min.js')}}"></script>

    @yield('script')
</div>
</body>
</html>
