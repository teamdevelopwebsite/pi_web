@extends('voyager::master')
@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)
@section('page_header')
<h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{
    ucfirst($dataType->display_name_singular) }} &nbsp;
</h1>
@include('voyager::multilingual.language-selector')
@stop
@section('content')

    @php
        $results=App\Result::where('quiz_id',$dataTypeContent->getKey())->get();
        $result_quiz=App\Result::where('quiz_id',$dataTypeContent->getKey())->count();
    @endphp
<div class="page-content read container-fluid">
    <div class="row">
        @if($result_quiz<=0)
        <div class="alert alert-danger text-center" role="alert">
           Waringing !!! Please Add Result of Quiz before - Add Question
        </div>
        @endif
        <div class="col-md-4">
            <h4 class="panel-heading">QUIZ
            </h4>
            <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-info">
                <span class="glyphicon glyphicon-list"></span>&nbsp;
                Quiz List
            </a>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">
                        View Result
                    </span>
                </button>
            <div class="collapse" id="collapseExample">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row"></div></div><div class="row"><div class="col-sm-12">

                                    <table id="dataTable" class="table table-hover dataTable no-footer" role="grid" aria-describedby="dataTable_info">
                                        <thead style="background-color: #F8FAFC">
                                        <tr><th>Result</th>
                                            <th>Image</th>
                                            <th style="float: right">Actions</th></tr>
                                        </thead>

                                        <tbody>
                                        @foreach($results as $item)
                                        <tr role="row" class="odd">
                                            <td>
                                                <div class="readmore" style="max-height: none;">{{$item->result}}</div>
                                            </td>
                                            <td>
                                                <img src="{{asset(Voyager::image($item->image))}}" style="width:100px">
                                            </td>
                                            <td class="no-sort no-click" id="bread-actions">
                                                <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete_result" data-id="{{$item->id}}" id="delete_result">
                                                    <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                                                </a>
                                                <a href="{{ route('voyager.'.'results'.'.edit', $item->id) }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                    <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                                </a>
                                            </td>
                                        </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <hr>
                                    <form action="{{ route('voyager.'.'results'.'.create') }}" method="GET">
                                        <input type="hidden" name="quiz_id" value="{{$dataTypeContent->getKey()}}">
                                        <button type="submit" class="btn btn-success add_result pull-left">
                                            <i class="voyager-plus"></i> <span class="hidden-xs hidden-sm">Add Result</span>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-bordered" style="padding-bottom:5px;">
                <!-- form start -->
                @foreach($dataType->readRows as $row)
                <div class="panel-heading" style="border-bottom:0;">
                    <h3 class="panel-title">{{ $row->display_name }}</h3>
                </div>
                <div class="panel-body" style="padding-top:0;">
                    @if($row->type == "image")
                    <img class="img-responsive" src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                    @elseif($row->type == 'multiple_images')
                    @if(json_decode($dataTypeContent->{$row->field}))
                    @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                    <img class="img-responsive" src="{{ filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file) }}">
                    @endforeach
                    @else
                    <img class="img-responsive" src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                    @endif
                    @elseif($row->type == 'relationship')
                    @include('voyager::formfields.relationship', ['view' => 'read', 'options' => $row->details])
                    @elseif($row->type == 'select_dropdown' && property_exists($row->details, 'options') &&
                    !empty($row->details->options->{$dataTypeContent->{$row->field}})
                    )
                    <?php echo $row->details->options->{$dataTypeContent->{$row->field}};?>
                    @elseif($row->type == 'select_dropdown' && $dataTypeContent->{$row->field . '_page_slug'})
                    <a href="{{ $dataTypeContent->{$row->field . '_page_slug'} }}">{{ $dataTypeContent->{$row->field}
                        }}</a>
                    @elseif($row->type == 'select_multiple')
                    @if(property_exists($row->details, 'relationship'))

                    @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                    @if($item->{$row->field . '_page_slug'})
                    <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last),
                    @endif
                    @else
                    {{ $item->{$row->field} }}
                    @endif
                    @endforeach

                    @elseif(property_exists($row->details, 'options'))
                    @if (count(json_decode($dataTypeContent->{$row->field})) > 0)
                    @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                    @if (@$row->details->options->{$item})
                    {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                    @endif
                    @endforeach
                    @else
                    {{ __('voyager::generic.none') }}
                    @endif
                    @endif
                    @elseif($row->type == 'date' || $row->type == 'timestamp')
                    {{ property_exists($row->details, 'format') ?
                    \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($row->details->format) :
                    $dataTypeContent->{$row->field} }}
                    @elseif($row->type == 'checkbox')
                    @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                    @if($dataTypeContent->{$row->field})
                    <span class="label label-info">{{ $row->details->on }}</span>
                    @else
                    <span class="label label-primary">{{ $row->details->off }}</span>
                    @endif
                    @else
                    {{ $dataTypeContent->{$row->field} }}
                    @endif
                    @elseif($row->type == 'color')
                    <span class="badge badge-lg" style="background-color: {{ $dataTypeContent->{$row->field} }}">{{
                        $dataTypeContent->{$row->field} }}</span>
                    @elseif($row->type == 'coordinates')
                    @include('voyager::partials.coordinates')
                    @elseif($row->type == 'rich_text_box')
                    @include('voyager::multilingual.input-hidden-bread-read')
                    <p>{!! $dataTypeContent->{$row->field} !!}</p>
                    @elseif($row->type == 'file')
                    @if(json_decode($dataTypeContent->{$row->field}))
                    @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                    <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}">
                        {{ $file->original_name ?: '' }}
                    </a>
                    <br />
                    @endforeach
                    @else
                    <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: '' }}">
                        {{ __('voyager::generic.download') }}
                    </a>
                    @endif
                    @else
                    @include('voyager::multilingual.input-hidden-bread-read')
                    <p>{{ $dataTypeContent->{$row->field} }}</p>
                    @endif
                </div><!-- panel-body -->
                @if(!$loop->last)
                <hr style="margin:0;">
                @endif
                @endforeach

            </div>
        </div>
        @php
        $results=App\QuizDetail::where('quiz_id',$dataTypeContent->getKey())->get();
        @endphp
        <div class="col-md-8">
            <h4 class="panel-heading">QUESTION
            </h4>
            <form action="{{ route('voyager.'.'quiz-details'.'.create', $dataTypeContent->getKey()) }}" method="get">
                <input type="hidden" name="quiz_id" value="{{$dataTypeContent->getKey()}}">
                <button class="btn btn-success" type="submit" {{ $result_quiz<=0 ? ' disabled' : '' }}> Add Question</button>
            </form>
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="row"></div></div><div class="row"><div class="col-sm-12">
                                <table id="dataTable" class="table table-hover dataTable no-footer" role="grid" aria-describedby="dataTable_info">
                                    <thead style="background-color: #F8FAFC">
                                    <th>Question</th>
                                    <th>Image</th>
                                    <th style="float: right">Actions</th></tr>
                                    </thead>
                                    <tbody>
                                    @if($results)
                                        @foreach($results as $item)
                                            <tr role="row" class="odd">
                                                <td>
                                                    <div class="readmore" style="max-height: none;">{{$item->question}}</div>
                                                </td>
                                                <td>
                                                    <img src="http://127.0.0.1:8000/storage/{{($item->image)}}" style="width:100px">
                                                </td>

                                                <td class="no-sort no-click" id="bread-actions">

                                                    <a href="javascript:;" title="Delete" class="btn btn-sm btn-danger pull-right delete" data-id="{{$item->id}}" id="delete">
                                                        <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                                                    </a>
                                                    <a href="{{ route('voyager.'.'quiz-details'.'.edit', $item->id) }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                        <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                                    </a>

                                                    <a href="/admin/quiz-details/{{$item->id}}"  class="btn btn-sm btn-warning pull-right ">
                                                        <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View</span>
                                                    </a>

                                                </td>
                                            </tr>
                                    <tbody>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>No Result</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

{{-- Single delete modal --}}
<div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{
                    strtolower($dataType->display_name_singular) }}?</h4>
            </div>
            <div class="modal-footer">
                <form action="{{ route('voyager.'.'quiz-details'.'.index') }}" id="delete_form" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->display_name_singular) }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{
                    __('voyager::generic.cancel') }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



{{-- Single delete modal --}}
<div class="modal modal-danger fade" tabindex="-1" id="delete_modal_result" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="voyager-trash"></i> Are you sure you want to delete this result?</h4>
            </div>
            <div class="modal-footer">
                <form action="{{ route('voyager.'.'results'.'.index') }}" id="delete_form_result" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->display_name_singular) }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{
                    __('voyager::generic.cancel') }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->







@stop

@section('javascript')
@if ($isModelTranslatable)
<script>
    $(document).ready(function () {
        $('.side-body').multilingual();
    });
</script>
<script src="{{ voyager_asset('js/multilingual.js') }}"></script>
@endif
<script>
    var deleteFormAction;
    $('.delete').on('click', function (e) {
        var form = $('#delete_form')[0];

        if (!deleteFormAction) {
            // Save form action initial value
            deleteFormAction = form.action;
        }

        form.action = deleteFormAction.match(/\/[0-9]+$/) ?
            deleteFormAction.replace(/([0-9]+$)/, $(this).data('id')) :
            deleteFormAction + '/' + $(this).data('id');

        $('#delete_modal').modal('show');
    });
</script>

<script>
    var deleteFormAction;
    $('.delete_result').on('click', function (e) {
        var form = $('#delete_form_result')[0];

        if (!deleteFormAction) {
            // Save form action initial value
            deleteFormAction = form.action;
        }

        form.action = deleteFormAction.match(/\/[0-9]+$/) ?
            deleteFormAction.replace(/([0-9]+$)/, $(this).data('id')) :
            deleteFormAction + '/' + $(this).data('id');

        $('#delete_modal_result').modal('show');
    });
</script>


@stop