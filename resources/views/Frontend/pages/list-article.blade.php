@extends('Frontend.main')

@section('content')

    @section('list-article')
        class = "active"
    @stop

    <!-- ##### Blog Post Area Start ##### -->
    <div class="viral-story-blog-post">

        <!-- Catagory Featured Post -->
        @foreach($catFeature as $cf)
        <div class="catagory-featured-post">
            <div class="container">
                <div class="row">
                    <!-- Catagory Thumbnail -->
                    <div class="col-12 col-md-7">
                        <div class="cata-thumbnail" style="height: 334px;overflow: hidden">
                            <a href="{{ route('article_detail.index', $cf->id) }}"><img src="{{ Voyager::image($cf->thumbnailImage) }}" style="object-fit: cover;height: 334px" alt=""></a>
                        </div>
                    </div>
                    <!-- Catagory Content -->
                    <div class="col-12 col-md-5">
                        <div class="cata-content">
                            <a href="{{ route('article.category', $cf->cat_id) }}" class="post-catagory">{{ $cf->cat_name }}</a>
                            <a href="{{ route('article_detail.index', $cf->id) }}">
                                <h2>{{ $cf->title }}</h2>
                            </a>
                            <div class="post-meta">
                                <p class="post-author">By <a href="#">{{ $cf->name }}</a></p>
                                <p class="post-date">{{ \App\Http\Controllers\Functions\FunctionController::generateTime($cf->date_diff) }}</p>
                            </div>
                            <p class="post-excerp">{!! html_entity_decode(str_limit($cf->post, 400)) !!}

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        <div class="container mt-50">
            <div class="row">
                <!-- Blog Posts Area -->
                <div class="col-12 col-lg-8">
                    <div class="row">
                        <!-- Single Blog Post -->
                        @foreach($allArt as $al)
                        <div class="col-12 col-lg-6">
                            <div class="single-blog-post style-3">
                                <!-- Post Thumb -->
                                <div class="post-thumb">
                                    <a href="{{ route('article_detail.index', $al->id) }}"><img src="{{ Voyager::image($al->thumbnailImage) }}" alt=""></a>
                                </div>
                                <!-- Post Data -->
                                <div class="post-data">
                                    <a href="{{ route('article.category', $al->cat_id) }}" class="post-catagory">{{ $al->cat_name }}</a>
                                    <a href="{{ route('article_detail.index', $al->id) }}" class="post-title">
                                        <h6>{{ $al->title }}</h6>
                                    </a>
                                    <div class="post-meta">
                                        <p class="post-author">By <a href="#">{{ $al->name }}</a></p>
                                        <p class="post-date"> {{ \App\Http\Controllers\Functions\FunctionController::generateTime($al->date_diff) }} </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach


                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="viral-news-pagination">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        <li class="page-item"><a class="page-link" href="#">01</a></li>
                                        <li class="page-item"><a class="page-link" href="#">02</a></li>
                                        <li class="page-item"><a class="page-link" href="#">03</a></li>
                                        <li class="page-item"><a class="page-link" href="#">04</a></li>
                                        <li class="page-item"><a class="page-link" href="#">05</a></li>
                                        <li class="page-item"><a class="page-link" href="#">...</a></li>
                                        <li class="page-item"><a class="page-link" href="#">15</a></li>
                                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Sidebar Area -->
                <div class="col-12 col-lg-4">
                    <div class="sidebar-area">

                        <!-- Trending Articles Widget -->
                        <div class="treading-articles-widget">
                            <h4>Trending Articles</h4>

                            <!-- Single Trending Articles -->
                            @foreach($topArt as $ta)
                            <div class="single-blog-post style-4 ">
                                <!-- Post Thumb -->
                                <div class="post-thumb">
                                    <a href="{{ route('article_detail.index', $ta->id) }}"><img src="{{ asset($ta->thumbnailImage) }}" alt=""></a>
                                    <span class="serial-number">{{ $ta->index }}</span>
                                </div>
                                <!-- Post Data -->
                                <div class="post-data">
                                    <a href="{{ route('article_detail.index', $ta->id) }}" class="post-title">
                                        <h6>{{ $ta->title }}</h6>
                                    </a>
                                    <div class="post-meta">
                                        <p class="post-author">By <a href="#">{{ $ta->name }}</a></p>
                                    </div>
                                </div>
                            </div>
                            @endforeach



                        </div>

                        <!-- Add Widget -->
                        <div class="add-widget mb-70">
                            <a href="#"><img src="{{asset('/images/blog-img/add.png')}}" alt=""></a>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Blog Post Area End ##### -->

@stop
@section('link_language')
    <div class="col-12 col-md-6 col-lg-4">
        <!-- change language Button -->
        <div class="classy-nav-container breakpoint-off" style="background:no-repeat !important;">
            <div class="container">
                <nav class="classy-navbar justify-content-between" id="viralnewsNav">
                    <div class="classy-menu"></div>
                    <div class="classynav">
                        <ul class="active">
                            <li><a href="#">CHANGE LANGUAGE</a>
                                @if(count($language) > 0)
                                    <ul class="dropdown">
                                        @foreach($language as $la)
                                            <li><a lang="en-kh" href="{{ route('article.language', $la->language_id) }}" hreflang="en-kh">{{ $la->language }}</a></li>
                                        @endforeach
                                    </ul>
                                @else
                                    <ul class="dropdown">
                                        <li>No alternative language with this article</li>
                                    </ul>
                                @endif
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
@stop
