@extends('Frontend.main')

@section('head')
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.11/css/perfect-scrollbar.css'>
    <link rel="stylesheet" href="{{asset('Frontend/css/VideosList/css/style.css')}}">
    <link rel="stylesheet" href="{{url('Frontend/css/css/font-awesome.min.css')}}">
@stop

@section('content')

    {{--<!-- ********** Hero Area End ********** -->--}}
    <div class="container section-padding-100">
        <div class="video-playlist-wrap">
            <div class="player-wrap">
                <div class="fluid-ratio-wrap">
                    <div class="fluid-ratio-inner">
                        <iframe width="100%" height="100%"
                                src="{{ $showVid[0]->url }}" frameborder="0"
                                allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="main-content-wrapper">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="post-content-area mt-15">
                                <h1 style="margin-bottom: 0"> {{ $showVid[0]->title }}</h1>
                                <span>Public on:<code> August 30,2018</code></span>
                                <div class="details">
                                    <span>{{ $showVid[0]->description }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="social-share-btns-container ">
                                <div class="social-share-btns">
                                  <a class="share-btn share-btn-twitter" href="https://twitter.com/intent/tweet?text={{ $showVid[0]->url }}/#" rel="nofollow" target="_blank">
                                    <i class="fab fa-twitter-square"></i>
                                    Tweet
                                  </a>
                                  <a class="share-btn share-btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $showVid[0]->url }}/#" rel="nofollow" target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                    Facebook
                                  </a>
                                  <a class="share-btn share-btn-linkedin" href="https://www.linkedin.com/cws/share?url={{ $showVid[0]->url }}/#" rel="nofollow" target="_blank">
                                    <i class="fab fa-linkedin"></i>
                                    Linkedin
                                  </a>
                                  <a class="share-btn share-btn-mail" href="mailto:?subject=Look Fun Codepen Account&amp;body={{ $showVid[0]->url }}#" rel="nofollow" target="_blank" title="via email">
                                    <i class="far fa-paper-plane"></i>
                                    Share
                                  </a>
                                </div>

                              </div>                              
                        </div>                      
                    </div>
                </div>
            </div>
            <hr>
            <div class="container">
                <div class="slider center">
                    <div class="clip">
                        <section>
                            <figure class="new-arrivals ">
                                <img class="" src="http://i.piccy.info/i9/57577700a9af30733f09462bf97a761d/1485778507/54789/1113648/8784_256x256x32_1_.png" title="image" alt="image">
                                <figcaption class="text-area">
                                    <h4 class="heading">Cat</h4>
                                    <h3 class="name">Description</h3>
                                </figcaption>
                            </figure>
                        </section>
                    </div>
                    <div class="clip">
                        <section>
                            <figure class="new-arrivals">
                                <img class="img-responsive" src="http://s1.iconbird.com/ico/2013/8/428/w256h2561377930213catbox.png" title="image" alt="image">
                                <figcaption class="text-area">
                                    <h4 class="heading">Cat</h4>
                                    <h3 class="name">Description</h3>
                                </figcaption>
                            </figure>
                        </section>
                    </div>
                    <div class="clip">
                        <section>
                            <figure class="new-arrivals ">
                                <img class="" src="http://s1.iconbird.com/ico/2013/8/428/w256h2561377930238catfish.png" title="image" alt="image">
                                <figcaption class="text-area">
                                    <h4 class="heading">Cat</h4>
                                    <h3 class="name">Description</h3>
                                </figcaption>
                            </figure>
                        </section>
                    </div>
                    <div class="clip">
                        <section>
                            <figure class="new-arrivals">
                                <img class="" src="http://s1.iconbird.com/ico/2013/8/428/w256h2561377930231cateyes.png" title="image" alt="image">
                                <figcaption class="text-area">
                                    <h4 class="heading">Cat</h4>
                                    <h3 class="name">Description</h3>
                                </figcaption>

                            </figure>
                        </section>
                    </div>
                    <div class="clip">
                        <section>
                            <figure class="new-arrivals">
                                <img class="" src="http://s1.iconbird.com/ico/2013/9/430/w256h2561378622493catupsidedown2.png" title="image" alt="image">
                                <figcaption class="text-area">
                                    <h4 class="heading">Cat</h4>
                                    <h3 class="name">Description</h3>
                                </figcaption>
                            </figure>
                        </section>
                    </div>
                    <div class="clip">
                        <section>
                            <figure class="new-arrivals ">
                                <img class="" src="http://www.free-icons-download.net/images/cat-mask-icon-83658.png" title="image" alt="image">
                                <figcaption class="text-area">
                                    <h4 class="heading">Cat</h4>
                                    <h3 class="name">Description</h3>
                                </figcaption>
                            </figure>
                        </section>
                    </div>
                </div>
            </div>
            <div class="playlist-wrap">
                <code>Related Videos</code>
                <section class="slider">
                    @foreach($relateVidTag as $rv)
                    <figure class="imghvr-push-up"><img src="">
                        <a href="/home">
                            <figcaption>
                                <h3>{{$rv -> title}}</h3>
                            </figcaption>
                        </a>
                    </figure>
                    @endforeach
                </section>
            </div>
        </div>
        <div class="video-playlist-wrap">
            <div class="playlist-wrap">
                <code>Trending Videos</code>
                <section class="slider">
                    @foreach($topVid as $tv)
                        <figure class="imghvr-push-up"><img src="">
                            <a href="/home">
                                <figcaption>

                                    <h3>{{$tv -> title}}</h3>
                                </figcaption>
                            </a>
                        </figure>
                    @endforeach
                </section>
            </div>
        </div>
    </div>
@section('script')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.11/js/min/perfect-scrollbar.jquery.min.js'></script>
    <script src='https://rawgit.com/kenwheeler/slick/master/slick/slick.js'></script>
    <script src="../Frontend/slick/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(document).on('ready', function() {
            $('.slider').slick({
                // centerMode:true,
                lazyLoad: 'ondemand',
                dots: true,
                centerPadding: '60px',
                slidesToShow: 5,
                arrows: true,
                // lazyLoad: 'progressive',
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: true,
                            // centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 5
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    </script>
@stop
@stop
