@extends('Frontend.main')
@section('head')
@stop
@section('content')
@section('home')
    class = "active"
@stop
<style>
.overlay-play-button{
            padding-bottom: 20px;
        }

        .overlay-play-button a img {
            display: block;
            position: relative;
            max-width: none;
           
        }
        .overlay-play-button:hover img{
            display: block;
            position: relative;
            max-width: none;
            width: calc(100%);
            -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
            transition: opacity 0.35s, transform 0.45s;
            -webkit-transform: scale(1.01);
            transform: scale(1.01);
            overflow: hidden;
        }
        .play-button{
            font-size: 14px;
            font-weight: 600;
            color: #fff;
            background-color: #e20378;
            width: 42px;
            height: 42px;
            border-radius: 50%;
            display: block;
            text-align: center;
            line-height: 42px;
            position: absolute;
            left: 42%;
            top: 33%;
            z-index: 10;
            filter: drop-shadow(2px 4px 6px black);
        }
        .play-button a i{
            color: #fff;
        }
</style>   
<div class="container">
    
    <section class="slider">

        <figure class="imghvr-push-up"><img src="http://cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Sport-News/International-Sports/soccer/Soccer54/Soccer125/5d10202a627a1_1561337880_large.jpg">
            <a href="/home">
            <figcaption>

                <h3>Hello World</h3>
                <p>Life is too important to be taken seriously!</p>

            </figcaption>
            </a>
        </figure>

        <figure class="imghvr-push-up"><img src="http://cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Ent-Int/Korea/Korea-102/5d009fb99c942_1560321960_large.jpg">
            <figcaption>
                <h3>Hello World</h3>
                <p>Life is too important to be taken seriously!</p>
            </figcaption><a href="javascript:;"></a>
        </figure>

        <figure class="imghvr-push-up"><img src="http://cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Sport-News/International-Sports/Freelancer-Sport/Football2/5d10da0433d46_1561385460_large.jpg">
            <figcaption>
                <h3>Hello World</h3>
                <p>Life is too important to be taken seriously!</p>
            </figcaption><a href="javascript:;"></a>
        </figure>

        <figure class="imghvr-push-up"> <img src="http://cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Technology-News/International/News265/5d0f7bce0c108_1561295820_large.jpg">
            <figcaption>
                <h3>Hello World</h3>
                <p>Life is too important to be taken seriously!</p>
            </figcaption><a href="javascript:;"></a>
        </figure>

        <figure class="imghvr-push-up">  <img src="http://cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Technology-News/International/News265/5d0f7c1093650_1561295880_large.jpg">
            <figcaption>
                <h3>Hello World</h3>
                <p>Life is too important to be taken seriously!</p>
            </figcaption><a href="javascript:;"></a>
        </figure>

        <figure class="imghvr-push-up">  <img src="http://cdn.sabay.com/cdn/media.sabay.com/media/Kanha/Alone/Alone-12/5c3d3f51eea3c_1547517720_large.jpg">
            <figcaption>
                <h3>Hello World</h3>
                <p>Life is too important to be taken seriously!</p>
            </figcaption><a href="javascript:;"></a>
        </figure>

    </section>
</div>
<!-- video section -->
<div class="container video-section">
    <a href="#"><h4>Most Popular Videos</h4></a>
    <div class="container">
        <!-- Modal Videos -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="max-width: 725px!important;">
                <div class="modal-content">
                    <div class="modal-body mb-0 p-0">
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <span class="mr-4">Spread the word!</span>
                        <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                        <!--Twitter-->
                        <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                        <!--Google +-->
                        <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                        <!--Linkedin-->
                        <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>
                        <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Grid row -->
        <div class="row">
        <!-- Grid column -->
        <div class="col-lg-4 col-md-12 mb-4">
            <!--Modal: Name-->
            <button type="button" class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/A3PDXmYoF5U" data-target="#myModal">
            <img class="img-fluid z-depth-1" src="https://mdbootstrap.com/img/screens/yt/screen-video-1.jpg" alt="video"></button>
        </div>
        <!-- Grid column -->
        <!-- Grid column -->
        <div class="col-lg-4 col-md-6 mb-4">
            <a><img class="img-fluid z-depth-1" src="https://mdbootstrap.com/img/screens/yt/screen-video-2.jpg" alt="video" data-toggle="modal" data-target="#modal1"></a>
        </div>
        <!-- Grid column -->
        <!-- Grid column -->
        <div class="col-lg-4 col-md-6 mb-4">
            <a><img class="img-fluid z-depth-1" src="https://mdbootstrap.com/img/screens/yt/screen-video-3.jpg" alt="video" data-toggle="modal" data-target="#modal1"></a>
        </div>
        <!-- Grid column -->
    </div>
    <!-- Grid row -->
    <div class="row">
        @foreach($topVid as $tv)
            <!-- Single Blog Post -->
            <div class="col-6 col-lg-3">
                <div class="single-blog-post style-3">
                    <!-- Post Thumb -->
                    <div class="post-thumb">
                        <!-- Embat video -->
                        <div class="embed-responsive overlay-play-button">
                            <img src="{{ asset($tv->thumbnailImage) }}" alt="">
                            <div class="play-button">
                                <a href="{{ route('video.show', $tv->id) }}"><i class="far fa-play-circle"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- Post Data -->
                    <div class="post-data">
                        <a href="#" class="post-catagory cat-4">{{ $tv->cat_name }}</a>
                        <a href="{{ route('video.show', $tv->id) }}" class="post-title">
                            <h6>{{ $tv->title }}</h6>
                        </a>
                        <div class="post-meta">
                            <p class="post-author">By <a href="#">{{ $tv->name }}</a></p>
                            <p class="post-date">{{ \App\Http\Controllers\Functions\FunctionController::generateTime($tv->date_diff) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

<!-- ##### Blog Post Area Start ##### -->
<div class="viral-story-blog-post mt-15">
    <div class="container">
        <div class="row">
            <!-- Blog Posts Area -->
            <div class="col-12 col-lg-9">
                <!-- Title -->
                <h3 href="#" class="post-title" style="border-bottom: #056894 solid 3px;padding: 0 10px;color: #056894">
                    Don’t Miss</h3>
                <div class="row">
                    <!-- Single Blog Post -->
                    @foreach($allArt as $al)
                    <div class="col-6 col-lg-4">
                        <div class="single-blog-post style-3">
                            <!-- Post Thumb -->
                            <div class="post-thumb">
                                <a href="{{ route('article_detail.index', $al->id) }}"><img src="{{ Voyager::image($al->thumbnailImage) }}" alt=""></a>
                            </div>
                            <!-- Post Data -->
                            <div class="post-data">
                                <a href="{{ route('article.category', $al->cat_id) }}" class="post-catagory">{{ $al->cat_name }}</a>
                                <a href="{{ route('article.index', $al->cat_id) }}" class="post-title">
                                    <h6>{{ $al->title }}</h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-author">By <a href="#">{{ $al->name }}</a></p>
                                    {{--<p class="post-date"> {{ $al->date_diff < 24 ? (int)$al->date_diff : (int)($al->date_diff / 24) < 30 ? (int)($al->date_diff / 24) : (int)($al->date_diff / (24*30)) < 12 ? (int)($al->date_diff / (24*30)) : (int)($al->date_diff / (24*30*12))}}--}}
                                        {{--{{ $al->date_diff < 24 ? "hour(s)" : (int)($al->date_diff / 24) < 30 ? "day(s)" : (int)($al->date_diff / (24*30)) < 12 ? "month(s)" : "year(s)" }} Ago</p>--}}
                                    <p class="post-date">{{ \App\Http\Controllers\Functions\FunctionController::generateTime($al->date_diff) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- Sidebar Area -->
            <div class="col-12 col-lg-3">
                <div class="sidebar-area">
                    <!-- Trending Articles Widget -->
                    <div class="treading-articles-widget">
                        <h4>Trending Articles</h4>

                        @foreach($topArt as $ta)
                        <!-- Single Trending Articles -->
                        <div class="single-blog-post style-4">
                            <!-- Post Thumb -->
                            <div class="post-thumb">
                                <a href="{{ route('article_detail.index', $ta->id) }}"><img src="{{ Voyager::image($ta->thumbnailImage) }}"
                                                                          alt=""></a>
                                <span class="serial-number">{{ $ta->index }}</span>
                            </div>
                            <!-- Post Data -->
                            <div class="post-data">
                                <a href="{{ route('article_detail.index', $ta->id) }}" class="post-title">
                                    <h6>{{ $ta->title }}</h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-author">By <a href="#">{{ $ta->name }}</a></p>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    <!-- Add Widget -->
                    <div class="add-widget mb-15">
                        <a href="#"><img src="{{asset('/images/blog-img/add.png')}}" alt=""></a>
                    </div>

                    <div class="footer-widget-area">
                        <!-- Widget Title -->
                        <h4 class="widget-title">Latest articles</h4>
                        @foreach($latestArt as $la)
                        <!-- Single Latest Post -->
                        <div class="single-blog-post style-2 d-flex align-items-center">
                            <div class="post-thumb">
                                <a href="{{ route('article_detail.index', $la->id) }}"><img style="height: 77px;object-fit: cover" src="{{ Voyager::image($la->thumbnailImage) }}"
                                                                          alt=""></a>
                            </div>
                            <div class="post-data">
                                <a href="{{url('/article-detail')}}" class="post-title">
                                    <h6>{{ $la->title }}</h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-date"><a href="#">7:00 AM | April 14</a></p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <!-- ##### Blog Post Area End ##### -->
@section('script')
{{--        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>--}}
{{--        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>--}}
        <script src="../Frontend/slick/slick.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript">
            $(document).on('ready', function() {
                $('.slider').slick({
                    // centerMode: true,
                    lazyLoad: 'ondemand',
                    dots: true,
                    centerPadding: '60px',
                    slidesToShow: 4,
                    arrows: true,
                    // lazyLoad: 'progressive',
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                arrows: true,
                                // centerMode: true,
                                centerPadding: '40px',
                                slidesToShow: 1
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                arrows: false,
                                centerMode: true,
                                centerPadding: '40px',
                                slidesToShow: 1
                            }
                        }
                    ]
                });
            });
            $('#modal1').on('hidden.bs.modal', function (e) {
                // do something...
                $('#modal1 iframe').attr("src", $("#modal1 iframe").attr("src"));
            });
        </script>
        <script>
            $(document).ready(function() {

// Gets the video src from the data-src on each button

                var $videoSrc;
                $('.video-btn').click(function() {
                    $videoSrc = $(this).data( "src" );
                });
                console.log($videoSrc);



// when the modal is opened autoplay it
                $('#myModal').on('shown.bs.modal', function (e) {

// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
                    $("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" );
                })



// stop playing the youtube video when I close the modal
                $('#myModal').on('hide.bs.modal', function (e) {
                    // a poor man's stop video
                    $("#video").attr('src',$videoSrc);
                })


                // http.onerror()=function(msg){
                //     reject(Error('network error' + msg))
                // }

                // $('spinner').show();
                // setTimeout(() => {
                //     http.send();
                // },5000);

                // var el=document.getElementById('diplay');

                // my_loade.the(function(msg){
                //     $('spinner').hide();
                //     el.innerText="succes-"+msg;
                // },function(msg){
                //     el.innerText = msg;
                // });
                // el.innerText = "loade all create"

// document ready
            });
        </script>

@stop
@stop
@section('link_language')
            <div class="col-12 col-md-6 col-lg-4">
                <!-- change language Button -->
                <div class="classy-nav-container breakpoint-off" style="background:no-repeat !important;">
                    <div class="container">
                        <nav class="classy-navbar justify-content-between" id="viralnewsNav">
                            <div class="classy-menu"></div>
                            <div class="classynav">
                                <ul class="active">
                                    <li><a href="#">CHANGE LANGUAGE</a>
                                        @if(count($language) > 0)
                                        <ul class="dropdown">
                                            @foreach($language as $la)
                                                <li><a lang="en-kh" href="{{ route('home.language', $la->id) }}" hreflang="en-kh">{{ $la->language }}</a></li>
                                            @endforeach
                                        </ul>
                                        @else
                                            <ul class="dropdown">
                                                <li>No alternative language with this article</li>
                                            </ul>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
@stop
