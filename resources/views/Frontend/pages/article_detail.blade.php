@extends('Frontend.main')

@section('content')

    <!-- ##### Viral News Breadcumb Area Start ##### -->
    <div class="viral-news-breadcumb-area section-padding-50">
        <div class="container h-100">
            <div class="row h-100 align-items-center">

                <!-- Breadcumb Area -->
                <div class="col-12 col-md-4">
                    <h3>Articles</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Articles</li>
                        </ol>
                    </nav>
                </div>

                <!-- Add Widget -->
                <div class="col-12 col-md-8">
                    <div class="add-widget">
                        <a href="#"><img src="/images/blog-img/add2.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Viral News Breadcumb Area End ##### -->

    <!-- ##### Blog Area Start ##### -->
    <div class="blog-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="blog-posts-area">
                        @foreach($art as $a)
                        <!-- Single Featured Post -->
                        <div class="single-blog-post-details">
                            <div class="post-thumb">
                                <img src="{{ asset($a->thumbnailImage) }}" alt="">
                            </div>
                            <div class="post-data">
                                <a href="#" class="post-catagory">{{ $a->cat_name }}</a>
                                <h2 class="post-title">{{ $a->title }}</h2>
                                <div class="post-meta">

                                    <!-- Post Details Meta Data -->
                                    <div class="post-details-meta-data mb-50 d-flex align-items-center justify-content-between">
                                        <!-- Post Author & Date -->
                                        <div class="post-authors-date">
                                            <p class="post-author">By <a href="#">{{ $a->name }}</a></p>
                                            <p class="post-date">{{ \App\Http\Controllers\Functions\FunctionController::generateTime($a->date_diff) }}</p>
                                        </div>
                                        <!-- View Comments -->
                                        <div class="view-comments">
                                            <p class="views">{{ $a->view_number }} Views</p>
                                            <a href="#comments" class="comments">3 comments</a>
                                        </div>
                                    </div>
                                    <p>{!!html_entity_decode($a->post)!!}
                                    </p>

                                    </div>
                            </div>
                            <!-- Login with Facebook to post comments -->
                            <div class="login-with-facebook my-5">
                                <a href="#">Login with Facebook to post comments</a>
                            </div>
                        </div>
                        @endforeach

                        <!-- Related Articles Area -->
                        <div class="related-articles-">
                            <h4 class="mb-70">Related Articles</h4>
                            @if(count($relateTag) > 0)
                            <div class="row">
                                <!-- Single Post -->
                                @foreach($relateTag as $rt)
                                <div class="col-12">
                                    <div class="single-blog-post style-3 style-5 d-flex align-items-center mb-50">
                                        <!-- Post Thumb -->
                                        <div class="post-thumb">
                                            <a href="{{ route('article_detail.index', $rt->id) }}"><img src="{{ asset($rt->thumbnailImage) }}" alt=""></a>
                                        </div>
                                        <!-- Post Data -->
                                        <div class="post-data">
                                            <a href="{{ route('article.category', $rt->cat_id) }}" class="post-catagory">{{ $rt->cat_name }}</a>
                                            <a href="{{ route('article_detail.index', $rt->id) }}" class="post-title">
                                                <h6>{{ $rt->title }}</h6>
                                            </a>
                                            <div class="post-meta">
                                                <p class="post-author">By <a href="#">{{ $rt->name }}</a></p>
                                                <p class="post-date">{{ \App\Http\Controllers\Functions\FunctionController::generateTime($rt->date_diff) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                            @endif
                        </div>

                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="blog-sidebar-area">

                        <!-- Newsletter Widget -->
                        <div class="newsletter-widget mb-70">
                            <h4>Sign up to <br>our newsletter</h4>
                            <form action="#" method="post">
                                <input type="text" name="text" placeholder="Name">
                                <input type="email" name="email" placeholder="Email">
                                <button type="submit" class="btn w-100">Subscribe</button>
                            </form>
                        </div>

                        <!-- Trending Articles Widget -->
                        <div class="treading-articles-widget mb-70">
                            <h4>Trending Articles</h4>
                            @if(count($topArt)>0)
                            @foreach($topArt as $ta)
                            <!-- Single Trending Articles -->
                            <div class="single-blog-post style-4">
                                <!-- Post Thumb -->
                                <div class="post-thumb">
                                    <a href="{{ route('article_detail.index', $ta->id) }}"><img src="{{ asset($ta->thumbnailImage) }}" alt=""></a>
                                    <span class="serial-number">{{ $ta->index }}</span>
                                </div>
                                <!-- Post Data -->
                                <div class="post-data">
                                    <a href="{{ route('article_detail.index', $ta->id) }}" class="post-title">
                                        <h6>{{ $ta->title }}</h6>
                                    </a>
                                    <div class="post-meta">
                                        <p class="post-author">By <a href="#">{{ $ta->name }}</a></p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif

                        </div>

                        <!-- Add Widget -->
                        <div class="add-widget mb-70">
                            <a href="#"><img src="/images/blog-img/add.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Blog Area End ##### -->
    <div class="container">
        <div class="main-content-wrapper section-padding-100">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <h4 class="title mb-70">comment</h4>
                    <!-- Comment Area Start -->
                    <div class="comment_area clearfix" id="comments">
                        @if(count($comment)>0)
                        <ol>
                            <!-- Single Comment Area -->
                            @foreach($comment as $com)
                            <li class="single_comment_area">
                                <!-- Comment Content -->
                                <div class="comment-content d-flex">
                                    <!-- Comment Author -->
                                    <div class="comment-author">
                                        <img src="/images/blog-img/t1.jpg" alt="author">
                                    </div>
                                    <!-- Comment Meta -->
                                    <div class="comment-meta">
                                        <a href="#" class="post-author">{{ $com->name }}</a>
                                        <a href="#" class="post-date">April 15, 2018</a>
                                        <p>{{ $com->comment }}</p>
                                    </div>
                                </div>
                                <ol class="children">
                                    <li class="single_comment_area">
                                        <!-- Comment Content -->
                                        <div class="comment-content d-flex">
                                            <!-- Comment Author -->
                                            <div class="comment-author">
                                                <img src="/images/blog-img/t2.jpg" alt="author">
                                            </div>
                                            <!-- Comment Meta -->
                                            <div class="comment-meta">
                                                <a href="#" class="post-author">Sandy Doe</a>
                                                <a href="#" class="post-date">April 15, 2018</a>
                                                <p>Donec turpis erat, scelerisque id euismod sit amet, fermentum vel dolor. Nulla facilisi. Sed pellen tesque lectus et accu msan aliquam. Fusce lobortis cursus quam, id mattis sapien.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                            </li>
                            @endforeach

                            <!-- Single Comment Area -->
                            <li class="single_comment_area">
                                <!-- Comment Content -->
                                <div class="comment-content d-flex">
                                    <!-- Comment Author -->
                                    <div class="comment-author">
                                        <img src="/images/blog-img/t3.jpg" alt="author">
                                    </div>
                                    <!-- Comment Meta -->
                                    <div class="comment-meta">
                                        <a href="#" class="post-author">Christian Williams</a>
                                        <a href="#" class="post-date">April 15, 2018</a>
                                        <p>Donec turpis erat, scelerisque id euismod sit amet, fermentum vel dolor. Nulla facilisi. Sed pellen tesque lectus et accu msan aliquam. Fusce lobortis cursus quam, id mattis sapien.</p>
                                    </div>
                                </div>
                            </li>
                        </ol>
                        @endif
                    </div>


                </div>
            </div>
        </div>
    </div>
@stop
@section('link_language')
    <div class="col-12 col-md-6 col-lg-4">
        <!-- change language Button -->
        <div class="classy-nav-container breakpoint-off" style="background:no-repeat !important;">
            <div class="container">
                <nav class="classy-navbar justify-content-between" id="viralnewsNav">
                    <div class="classy-menu"></div>
                    <div class="classynav">
                        <ul class="active">
                            <li><a href="#">CHANGE LANGUAGE</a>
                                @if(count($language) > 0)
                                <ul class="dropdown">
                                    @foreach($language as $la)
                                        <li><a lang="en-kh" href="{{ route('article_detail.translate', ['article_id'=>$art[0]->id, 'lang_id'=>$la->lang_id]) }}" hreflang="en-kh">{{ $la->language }}</a></li>
                                    @endforeach
                                </ul>
                                @else
                                <ul class="dropdown">
                                    <li>No alternative language with this article</li>
                                </ul>
                                @endif
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
@stop
