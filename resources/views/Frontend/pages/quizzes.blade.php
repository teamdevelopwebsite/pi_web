@extends('Frontend.main')

@section('content')
@section('quizzes')
    class= "active"
@stop

<!-- ##### Blog Post Area Start ##### -->
<div class="viral-story-blog-post section-padding-0-50">

    <!-- Catagory Featured Post -->


    <div class="container">
        <div class="row" style="margin-top: 20px">
            <!-- Catagory Thumbnail -->
            <div class="col-12 col-md-8">
                <div class="cata-thumbnail">
                    <a href="{{ route('quiz.show', $quizs_feature->id) }}"><img
                                src="{{Voyager::image($quizs_feature->image)}}" style="height: 334px;object-fit: cover" alt=""></a>
                </div>
            </div>
            <!-- Catagory Content -->
            <div class="col-12 col-md-4">
                <div class="cata-content">
                    <a href="{{ route('quiz.show', $quizs_feature->id) }}">
                        <h4>{{ $quizs_feature->title }}</h4>
                    </a>
                    <div class="post-meta">
                        <p class="post-author">By <a href="#">{{$quizs_feature->name}}</a></p>
                        <p class="post-date">{{$quizs_feature->created_at}}

                        </p>
                    </div>
                    <p class="post-excerp">{{$quizs_feature->description}}</p>
                </div>
            </div>

        </div>
        <div class="row" style="margin-top: 20px">

            <!-- Blog Posts Area -->
            <div class="col-12 col-lg-8">
                <div class="row">
                @foreach($quizs as $q)
                    <!-- Single Blog Post -->
                        <div class="col-12 col-lg-6">
                            <div class="single-blog-post style-3">
                                <!-- Post Thumb -->
                                <div class="post-thumb">
                                    <a href="{{ route('quiz.show', $q->id) }}"><img src="{{ asset($q->image) }}" alt=""></a>
                                    <img src="{{Voyager::image($q->image)}}"/>
                                </div>
                                <!-- Post Data -->
                                <div class="post-data">
                                    {{--<a href="#" class="post-catagory">Finance</a>--}}
                                    <a href="{{ route('quiz.show', $q->id) }}" class="post-title">
                                        <h6>{{ $q->title }}</h6>
                                    </a>
                                    <div class="post-meta">
                                        <p class="post-author">By <a href="#">Michael Smithson</a></p>
                                        <p class="post-date">5 Hours Ago</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="viral-news-pagination">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item">{{ $quizs->links() }}</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Sidebar Area -->
            <div class="col-12 col-lg-4">
                <div class="sidebar-area">

                    <!-- Trending Articles Widget -->
                    <div class="treading-articles-widget mb-70">
                        <h4>Trending Articles</h4>

                        <!-- Single Trending Articles -->
                        <div class="single-blog-post style-4">
                            <!-- Post Thumb -->
                            <div class="post-thumb">
                                <a href="#"><img src="/images/blog-img/15.jpg" alt=""></a>
                                <span class="serial-number">01.</span>
                            </div>
                            <!-- Post Data -->
                            <div class="post-data">
                                <a href="#" class="post-title">
                                    <h6>This Is How Notebooks Of An Artist Who Travels Around The World Look</h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-author">By <a href="#">Michael Smithson</a></p>
                                </div>
                            </div>
                        </div>

                        <!-- Single Trending Articles -->
                        <div class="single-blog-post style-4">
                            <!-- Post Thumb -->
                            <div class="post-thumb">
                                <a href="#"><img src="/images/blog-img/16.jpg" alt=""></a>
                                <span class="serial-number">02.</span>
                            </div>
                            <!-- Post Data -->
                            <div class="post-data">
                                <a href="#" class="post-title">
                                    <h6>Artist Recreates People’s Childhood Memories With Realistic Dioramas</h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-author">By <a href="#">Michael Smithson</a></p>
                                </div>
                            </div>
                        </div>

                        <!-- Single Trending Articles -->
                        <div class="single-blog-post style-4">
                            <!-- Post Thumb -->
                            <div class="post-thumb">
                                <a href="#"><img src="/images/blog-img/17.jpg" alt=""></a>
                                <span class="serial-number">03.</span>
                            </div>
                            <!-- Post Data -->
                            <div class="post-data">
                                <a href="#" class="post-title">
                                    <h6>Artist Recreates People’s Childhood Memories With Realistic Dioramas</h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-author">By <a href="#">Michael Smithson</a></p>
                                </div>
                            </div>
                        </div>

                    </div>


                    <!-- Latest Comments -->
                    <div class="latest-comments-widget">
                        <h4>Latest Comments</h4>

                        <!-- Single Comment Widget -->
                        <div class="single-comments d-flex">
                            <div class="comments-thumbnail">
                                <img src="/images/blog-img/t1.jpg" alt="">
                            </div>
                            <div class="comments-text">
                                <a href="#"><span>Jamie Smith</span> on Facebook is offering facial recognition...</a>
                                <p>06:34 am, April 14, 2018</p>
                            </div>
                        </div>

                        <!-- Single Comment Widget -->
                        <div class="single-comments d-flex">
                            <div class="comments-thumbnail">
                                <img src="/images/blog-img/t2.jpg" alt="">
                            </div>
                            <div class="comments-text">
                                <a href="#"><span>Tania Heffner</span> on Facebook is offering facial recognition...</a>
                                <p>06:34 am, April 14, 2018</p>
                            </div>
                        </div>

                        <!-- Single Comment Widget -->
                        <div class="single-comments d-flex">
                            <div class="comments-thumbnail">
                                <img src="/images/blog-img/t3.jpg" alt="">
                            </div>
                            <div class="comments-text">
                                <a href="#"><span>Sandy Doe</span> on Facebook is offering facial recognition...</a>
                                <p>06:34 am, April 14, 2018</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Blog Post Area End ##### -->

@stop
