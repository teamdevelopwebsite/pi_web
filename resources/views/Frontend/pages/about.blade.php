@extends('Frontend.main')

@section('content')
@section('about')
    class= "active"
@stop
    <style>
        .white_svg {
            position: relative;
            left: 0;
            bottom: -11px;
            width: 100%;
            overflow: hidden;
        }

        .svg_white svg {
            fill: #fff;
            width: 101%;
        }

        .pink_svg {
            position: relative;
            left: 0;
            bottom: -11px;
            width: 100%;
            overflow: hidden;
        }

        .svg_pink svg {
            fill: #e20378;
            width: 101%;
        }

        .abt-left-thumb {
            display: block;
            position: relative;
            z-index: 1;
            box-shadow: 0px 0px 21px rgba(0,0,0,0.3);
        }
        .abt-left-thumb img {
            border-radius: 4px;
            overflow: hidden;
            max-width: 100%;
        }

        .abt-left-thumb:before {
            content: '';
            position: absolute;
            right: -45px;
            bottom: -45px;
            border: 2px solid #f3f2f2;
            height: 100%;
            width: 100%;
            z-index: -1;
            border-radius: 0px 0px 30px 0px;
        }
        .section-title h2 {
            font-size: 29px;
            letter-spacing: 0;
            color: #222222;
            line-height: 30px;
            margin-bottom: 50px;
            padding-bottom: 16px;
            position: relative;
        }

        .social-link {
            display: inline-block;
            margin-top: 11px;
        }

        .social-link h5 {
            font-family: 'Montserrat', sans-serif;
            display: inline-block;
            font-size: 16px;
            letter-spacing: 0;
            font-weight: 600;
            color: #222222;
            margin-right: 22px;
        }

        .social-link a {
            display: inline-block;
            font-size: 15px;
            color: #333333;
            margin-right: 11px;
        }
        .fa {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
    </style>

    <div id="preloader">
        <div class="preload-content">
            <div id="world-load"></div>
        </div>
    </div>

    <section id="home" class="slider-area parallax" data-speed="3" data-bg-image="assets/img/bg/slider-bg.jpg" style="background-image: url({{asset('images/core-img/background-black-background-card.jpg')}}); background-position: 50% ; background-size: cover;">
        <div class="container">
            <div class="slider-content">
                <a href="/"><img src="{{ asset('/images/core-img/iEx51.png') }}" alt="">
                </a>
            </div>
        </div>
        <div class="white_svg svg_white">
            <svg x="0px" y="0px" viewBox="0 186.5 1920 113.5">
                <polygon points="0,300 655.167,210.5 1432.5,300 1920,198.5 1920,300 "></polygon>
            </svg>
        </div>
    </section>

    <div class="white_svg svg_white">
        <svg x="0px" y="0px" viewBox="0 186.5 1920 113.5">
            <polygon points="0,300 655.167,210.5 1432.5,300 1920,198.5 1920,300 "></polygon>
        </svg>
    </div>

    <section class="about-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <div class="abt-left-thumb">
                        <img src="{{ asset('images/core-img/1.jpg') }}" alt="image">
                    </div>
                </div>
                <div class="col-md-7 offset-md-1">
                    <div class="abt-content">
                        <div class="section-title">
                            <h2 class="txt2_is_show is_show">About Us</h2>
                        </div>
                        <p class="txt1-wrap"><span class="txt1" style="transform: translate3d(0px, 0%, 0px); opacity: 1;">For instance, whenever I go back to the guest house during the morning to copy out the contract, these gentlemen are always still sitting there eating their breakfasts. I ought to just try that witht my boss; I'd get kicked out on the spot.</span></p>
                        <p class="txt1-wrap"><span class="txt1" style="transform: translate3d(0px, 0%, 0px); opacity: 1;">But who knows, maybe that would be the best thing for me. He'd fall right off his desk! And it's a funny sort of business to be sitting up there at your desk,</span> <span class="txt1" style="transform: translate3d(0px, 0%, 0px); opacity: 1;">talking down at your subordinates. I ought to just try that witht my boss; I'd get kicked out on the spot. But who knows, maybe that would be the best thing for me. He'd fall right off his desk! And it's a funny sort of business to be sitting up there at your desk, talking down at your subordinates.</span></p>
                        <div class="social-link row">
                            <h5>STAY WITH US :</h5>
                            <div class="col-12">
                                <div class="social-share-btns-container ">
                                    <div class="social-share-btns">
                                        <a class="share-btn share-btn-facebook" href="https://www.facebook.com/iextysk/" rel="nofollow" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                        Facebook
                                        </a>
                                        <a class="share-btn share-btn-mail" href="mailto:?subject=Look Fun Codepen Account&amp;body= iextysk@gmail.com" rel="nofollow" target="_blank" title="via email">
                                            <i class="fas fa-envelope-open"></i>
                                        Share
                                        </a>
                                    </div>
    
                                    </div>                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    <div class="pink_svg svg_pink">
        <svg x="0px" y="0px" viewBox="0 186.5 1920 113.5">
            <polygon points="0,300 655.167,210.5 1432.5,300 1920,198.5 1920,300 "></polygon>
        </svg>
    </div>

@stop

@section('link_language')
    {{--{{ route('about') }}--}}
@stop
