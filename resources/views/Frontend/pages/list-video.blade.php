@extends('Frontend.main')

@section('head')

    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.11/css/perfect-scrollbar.css'>
    <link rel='stylesheet' href='https://rawgit.com/kenwheeler/slick/master/slick/slick.css'>
    <link rel='stylesheet' href='https://rawgit.com/kenwheeler/slick/master/slick/slick-theme.css'>
    <link rel="stylesheet" href="{{asset('Frontend/css/VideosList/css/style.css')}}">
    <link rel="stylesheet" href="{{url('Frontend/css/css/font-awesome.min.css')}}">
    <style type="text/css">
        .video-playlist-wrap .player-wrap .fluid-ratio-wrap .fluid-ratio-inner iframe {
            width: 100% !important;
            height: 100% !important;
        }

        .video-playlist-wrap {
            padding: 0;
        }

        hr {
            padding: 5px 0;
        }

        .justify-content-center {
            margin-top: 10px;
        }

        .video-playlist-wrap {
            padding-top: 0 !important;
        }

        .video-playlist-wrap .player-wrap .fluid-ratio-wrap .fluid-ratio-inner iframe {
            width: 100% !important;
            height: 100% !important;
        }

        .title {
            padding-left: 0 !important;
        }

        .post-sidebar-area {
            border: none;
        }

        .sidebar-widget-area .widget-content {
            padding-top: 5px !important;
            padding-bottom: 5px !important;
        }

        .padding-side-12 {
            padding-left: 12px;
            padding-right: 12px;
        }

        .single-blog-post:hover .post-thumbnail img {
            transform: scale(1.2);
            transition: 1s;
        }

        .single-blog-post .post-thumbnail img {
            /*transform: scale(1.5);*/
            transition: 1s;
        }

        .post-thumbnail {
            overflow: hidden;
        }

        .single-blog-post:hover {
            /* box-shadow: 0 10px 10px rgba(0, 0, 0, 0.15); */
        }

        .single-blog-post-trending a img{
            height: 150px;
            object-fit: cover;
        }
        .single-blog-post-trending .post-thumb {
            position: relative;
            z-index: 10;
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            min-width: 100%;
            width: 100%;
            margin-bottom: 15px;
        }
        .single-blog-post-trending.style-9 .post-thumb .serial-number {
            font-size: 14px;
            font-weight: 600;
            color: #fff;
            background-color: #e20378;
            width: 42px;
            height: 42px;
            border-radius: 50%;
            display: block;
            text-align: center;
            line-height: 42px;
            position: absolute;
            left: 15px;
            top: -200px;
            z-index: 10;
        }
/* hovereffect css section */
        .single-blog-post-trending {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        /* background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%); */
        }

        .single-blog-post-trending .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
        }

        .single-blog-post-trending img {
        display: block;
        position: relative;
        max-width: none;
        width: calc(100% + 60px);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-40px,0,0);
        transform: translate3d(-40px,0,0);
        }

        .single-blog-post-trending h2 {
        text-transform: uppercase;
        color: #FF6346;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 15% 0 10px 0;
        text-align: left;
        }

        .single-blog-post-trending .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 2px solid #FF6346;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
        }

        .single-blog-post-trending a, .single-blog-post-trending p {
        color: #FF6346;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        }

        .single-blog-post-trending:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
        }

        .single-blog-post-trending:hover .overlay:before,
        .single-blog-post-trending:hover a, .single-blog-post-trending:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
        }

        .overlay-play-button{
            padding-bottom: 20px;
        }

        .overlay-play-button a img {
            display: block;
            position: relative;
            max-width: none;
           
        }
        .overlay-play-button:hover img{
            display: block;
            position: relative;
            max-width: none;
            width: calc(100%);
            -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
            transition: opacity 0.35s, transform 0.45s;
            -webkit-transform: scale(1.01);
            transform: scale(1.01);
            overflow: hidden;
        }
        .play-button{
            font-size: 14px;
            font-weight: 600;
            color: #fff;
            background-color: #e20378;
            width: 42px;
            height: 42px;
            border-radius: 50%;
            display: block;
            text-align: center;
            line-height: 42px;
            position: absolute;
            left: 42%;
            top: 33%;
            z-index: 10;
            filter: drop-shadow(2px 4px 6px black);
        }
        .play-button a i{
            color: #fff;
        }
    </style>
@stop

@section('content')
    @section('videos')
        class= "active"
    @stop

    <div class="container">
        <div><h2>videos</h2></div>
        <hr style="margin: 0">
        <div class="video-playlist-wrap two-col">

            <div class="player-wrap">
                <div class="fluid-ratio-wrap">
                    <div class="fluid-ratio-inner">
                        <iframe src="{{url($featureVid[0]->url)}}"
                                frameborder="0" allowfullscreen allow="autoplay"></iframe>
                    </div>
                </div>
            </div>

            <div class="playlist-wrap">
                <ul class="remove-bullets scroll-wrap">
                    @foreach($relateVidTag as $rv)
                    <div class="col-12">
                        <div class="overlay-play-button">
                            <a href="{{ route('video.show', $rv->id) }}"><img class="img-responsive" src="{{ asset($rv->thumbnailImage) }}" alt=""></a>
                            <div class="play-button">
                                <a href="{{ route('video.show', $rv->id) }}"><i class="far fa-play-circle"></i></a>
                            </div>
                        </div>

                    </div>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="video-playlist-wrap">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="post-content-area mt-15">
                        <h3 style="margin-bottom: 0">{{ $featureVid[0]->title }}</h3>
                        <span>Public on:<code> August 30,2018</code></span>
                    </div>
                    <div class="details">
                        <span>{{ $featureVid[0]->description }}</span>
                    </div>
                </div>
                <div class="col-12">
                    <div class="social-share-btns-container ">
                        <div class="social-share-btns">
                            <a class="share-btn share-btn-twitter" href="https://twitter.com/intent/tweet?text={{url($featureVid[0]->url)}}/#" rel="nofollow" target="_blank">
                            <i class="fab fa-twitter-square"></i>
                            Tweet
                            </a>
                            <a class="share-btn share-btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{url($featureVid[0]->url)}}/#" rel="nofollow" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                            Facebook
                            </a>
                            <a class="share-btn share-btn-linkedin" href="https://www.linkedin.com/cws/share?url={{url($featureVid[0]->url)}}/#" rel="nofollow" target="_blank">
                            <i class="fab fa-linkedin"></i>
                            Linkedin
                            </a>
                            <a class="share-btn share-btn-mail" href="mailto:?subject=Look Fun Codepen Account&amp;body={{url($featureVid[0]->url)}}#" rel="nofollow" target="_blank" title="via email">
                            <i class="far fa-paper-plane"></i>
                            Share
                            </a>
                        </div>
                    </div>                              
                </div>
            </div>
            <hr>
        </div>
        <!-- ##### Blog Post Area Start ##### -->
        <div class="viral-story-blog-post section-padding-0-50">
            
            <div class="container">
                <div class="row">
                    <!-- Blog Posts Area -->
                    <div class="col-12 col-lg-8">
                        <div class="row">

                            <!-- Single Blog Post -->
                            @foreach($relateVidTag as $rv)
                            <div class="col-12 col-lg-6">
                                <div class="single-blog-post style-3">
                                    <!-- Post Thumb -->
                                    <div class="post-thumb">
                                        <img src="{{ asset($rv->thumbnailImage) }}" alt="">
                                    </div>
                                    <div class="play-button">
                                        <a href="{{ route('video.show', $rv->id) }}"><i class="far fa-play-circle"></i></a>
                                    </div>
                                    <!-- Post Data -->
                                    <div class="post-data">
                                        <a href="#" class="post-catagory">{{ $rv->cat_name }}</a>
                                        <a href="{{ route('video.show', $rv->id) }}" class="post-title">
                                            <h6>{{ $rv->title }}</h6>
                                        </a>
                                        <div class="post-meta">
                                            <p class="post-author">By <a href="#">{{ $rv->name }}</a></p>
                                            <p class="post-date">{{ \App\Http\Controllers\Functions\FunctionController::generateTime($rv->date_diff) }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="viral-news-pagination">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination">
                                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                            <li class="page-item"><a class="page-link" href="#">01</a></li>
                                            <li class="page-item"><a class="page-link" href="#">02</a></li>
                                            <li class="page-item"><a class="page-link" href="#">03</a></li>
                                            <li class="page-item"><a class="page-link" href="#">04</a></li>
                                            <li class="page-item"><a class="page-link" href="#">05</a></li>
                                            <li class="page-item"><a class="page-link" href="#">...</a></li>
                                            <li class="page-item"><a class="page-link" href="#">15</a></li>
                                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Sidebar Area -->
                    <div class="col-12 col-lg-4">
                        <div class="sidebar-area">
             
                            <!-- Trending Articles Widget -->
                            <div class="treading-articles-widget mb-70">
                                <!-- Ads Widget -->
                                <div class="add-widget mb-70">
                                    <a href="#"><img src="images/blog-img/add.png" alt=""></a>
                                </div>
                                
                                <h4 class=" mb-70">Trending Video</h4>
                                @foreach($topVid as $tv)
                                <div class="single-blog-post-trending style-9">
                                    <img class="img-responsive" src="{{ asset($tv->thumbnailImage) }}" alt="">
                                    <div class="overlay">
                                        <h2>{{ $tv->title }}</h2>
                                        <h2><a href="{{ route('video.show', $tv->id) }}"><i class="fas fa-play-circle"></i>PLAY HERE</a></h2>
                                        {{-- <p class="post-author">By <a href="#">{{ $tv->name }}</a></p>  --}}
                                    </div> 
                                    <div class="post-thumb">
                                        <span class="serial-number">{{ $tv->index }}</span>
                                    </div>  
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ##### Blog Post Area End ##### -->
    </div>

@section('link_language')

    <div class="col-12 col-md-6 col-lg-4">
        <!-- change language Button -->
        <div class="classy-nav-container breakpoint-off" style="background:no-repeat !important;">
            <div class="container">
                <nav class="classy-navbar justify-content-between" id="viralnewsNav">
                    <div class="classy-menu"></div>
                    <div class="classynav">
                        <ul class="active">
                            <li><a href="#">CHANGE LANGUAGE</a>
                                <ul class="dropdown">
                                    @foreach($language as $la)
                                        <li><a lang="en-kh" href="#" hreflang="en-kh">{{ $la->language }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>

@stop
@section('script')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.11/js/min/perfect-scrollbar.jquery.min.js'></script>
    <script src='https://rawgit.com/kenwheeler/slick/master/slick/slick.js'></script>
    <script type="text/javascript" src="{{asset('Frontend/css/VideosList/js/index.js')}}"></script>

@stop
@stop
