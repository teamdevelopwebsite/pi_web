@extends('Frontend.main')

@section('head')
    <link rel="stylesheet" href="{{ asset('Frontend/css/quiz/quiz.css') }}">
    <style type="text/css">
        h1 {
            margin: 0 0 20px 0;
            font-weight: 300;
            font-size: 28px;
        }

        input[type="text"],
        input[type="password"] {
            display: block;
            box-sizing: border-box;
            margin-bottom: 20px;
            padding: 4px;
            width: 100%;
            height: 32px;
            border: none;
            border-bottom: 1px solid #AAA;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            font-size: 15px;
            transition: 0.2s ease;
        }

        input[type="date"] {
            display: block;
            box-sizing: border-box;
            margin-bottom: 20px;
            padding: 4px;
            width: 220px;
            height: 32px;
            border: none;
            border-bottom: 1px solid #AAA;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            font-size: 15px;
            transition: 0.2s ease;

        }

        input[type="date"]:focus {
            border-bottom: 2px solid #16a085;
            color: #16a085;
            transition: 0.2s ease;
        }

        input[type="text"]:focus,
        input[type="password"]:focus {
            border-bottom: 2px solid #16a085;
            color: #16a085;
            transition: 0.2s ease;
        }

        input[type="submit"] {
            width: 100%;
            height: 32px;
            background: #16a085;
            border: none;
            border-radius: 2px;
            color: #FFF;
            font-family: 'Roboto', sans-serif;
            font-weight: 500;
            text-transform: uppercase;
            transition: 0.1s ease;
            cursor: pointer;
        }

        input[type="submit"]:hover,
        input[type="submit"]:focus {
            opacity: 0.8;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
            transition: 0.1s ease;
        }

        input[type="submit"]:active {
            opacity: 1;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
            transition: 0.1s ease;
        }

        input[type="button"] {
            margin-top: 28px;
            width: 100px;
            height: 32px;
            background: #16a085;
            border: none;
            border-radius: 2px;
            color: #FFF;
            font-family: 'Roboto', sans-serif;
            font-weight: 500;
            text-transform: uppercase;
            transition: 0.1s ease;
            cursor: pointer;
        }

        input[type="button"]:hover,
        input[type="button"]:focus {
            opacity: 0.8;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
            transition: 0.1s ease;
        }

        input[type="button"]:active {
            opacity: 1;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
            transition: 0.1s ease;
        }

        .or {
            position: absolute;
            top: 225px;
            left: 280px;
            width: 40px;
            height: 40px;
            background: #DDD;
            border-radius: 50%;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
            line-height: 40px;
            text-align: center;
        }

        .element {
            display: inline-flex;
        }

        .element .group {
            display: inline-block;
            position: relative;
            cursor: pointer;
            margin: 0px;
        }

        .element button {
            padding: 10px 20px;
            font-size: 16px;
            border-radius: 2px;
            cursor: inherit;
            border: 0;
            color: #ffffff;
            width: 150px;
            outline: 0;
            font-family: inherit;
            -webkit-transition: background-color 0.2s ease-in-out;
            -moz-transition: background-color 0.2s ease-in-out;
            -o-transition: background-color 0.2s ease-in-out;
            transition: background-color 0.2s ease-in-out;
        }

        .fb {
            background-color: #306199;
        }

        .fb:hover {
            background-color: #244872;
        }

        .gp {
            background-color: #e93f2e;
        }

        .gp:hover {
            background-color: #ce2616;
        }
    </style>
    <head>
        @stop
        @section('content')
{{--         
            <div class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Modal body text goes here.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div> --}}

            <div class="main-content-wrapper section-padding-40">
                <div class="container">
                    <div class="row justify-content-center">
                        <!-- ============= Quiz Area  ============= -->
                        <div class="col-12 col-lg-8">
                            <div class="post-content-area mb-100">
                                <!-- Title quiz -->
                                <a href="#">
                                    <h2>{{ $quizs[0]->title }}</h2>
                                </a>
                                <!-- Post Content -->
                                <div class="post-content">
                                    @if($quizs[0]->description != null)
                                        <p>{{ $quizs[0]->description }}</p>
                                    @endif
                                    <div class="post-meta">
                                        <p>
                                            <a href="#" class="post-author">{{ $user[0]->name }} </a> Post on <a href="#"class="post-date">Sep 29, 2017 at 9:48 am</a>
                                        </p>
                                    </div>
                                </div>
                                <hr>
                                <!-- Question Image -->
                                @foreach($quiz_detail as $qd)
                                    <br>
                                    <div class="qs_img">
                                        <img src="{{ Voyager::image($qd->image) }}" width='1000'>
                                    </div>
                                    <!-- Answer image -->
                                    <div class="row" style="margin-top: 10px;align-items: center">
                                        <form class="col-12">
                                            @foreach($answer as $an)
                                                @if($an->qdetail_id == $qd->id)
                                                    {{csrf_field()}}
                                                    @if(Auth::guard('userclients')->guest())
                                                        <input onclick="test({{$an->id}},{{$qd->id}})" class="zhidden"
                                                               name="image-pick[]"
                                                               value={{ $an->id }} type={{ $quizs[0]->multiAns==0 ? "radio" : "checkbox" }}
                                                                       id={{ $an->id }} data-toggle="modal"
                                                               data-target="#exampleModalCenter">
                                                        <label for={{ $an->id }}>
                                                            <img src="{{ Voyager::image($an->image) }}">
                                                        </label>
                                                    @elseif(Auth::guard('userclients')->check())
                                                        <input onclick="test({{$an->id}},{{$qd->id}})" class="hidden"
                                                               name="image-pick[]"
                                                               value={{ $an->id }} type={{ $quizs[0]->multiAns==0 ? "radio" : "checkbox" }}
                                                                       id={{ $an->id }}>
                                                        <label for={{ $an->id }}>
                                                            <img src="{{ Voyager::image($an->image) }}">
                                                        </label>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </form>
                                        <br>
                                        <h2 id="selected"></h2>
                                    </div>
                                @endforeach
                                <br>
                                <hr>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="login-page">
                                                <div class="form">
                                                    <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                                                    <h1>Log In</h1>
                                                    <form id="formRegister" class="login-form" method="POST"
                                                          action="{{ url('/login_quiz') }}">
                                                        {{csrf_field()}}
                                                        <input type="text" name="email" id="email"
                                                               placeholder="Please Enter your email"/>
                                                        <input type="password" name="password" id="password"
                                                               placeholder="Please Enter your password"/>
                                                        <button type="submit">login</button>
                                                        <button type="button"
                                                                style="background-color: red;color: white; margin-top: 5px"
                                                                data-dismiss="modal">Cancel
                                                        </button>
                                                        <p class="message">Not registered?
                                                            <a href="{{ url('/register')}}" data-toggle="modal"
                                                               data-target="#exampleModalRegister">Create an
                                                                account</a>
                                                        </p>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if(Auth::guard('userclients')->check())
                                    <div class="login-with-facebook my-5">
                                        <a href="#" onclick="submitAnswer()">Show Result</a>
                                    </div>
                                @elseif(Auth::guard('userclients')->guest())
                                @endif
                            <!-- Modal Result -->
                                <div class="modal fade" id="resultModel" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <form class="px-4 py-3">
                                                    <div class="form-group">
                                                        <label for="exampleDropdownFormEmail1">Email address</label>
                                                        <input type="email" class="form-control"
                                                               id="exampleDropdownFormEmail1"
                                                               placeholder="email@example.com">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleDropdownFormPassword1">Password</label>
                                                        <input type="password" class="form-control"
                                                               id="exampleDropdownFormPassword1" placeholder="Password">
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <input type="checkbox" class="form-check-input"
                                                                   id="dropdownCheck">
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Exit
                                                    </button>
                                                    <button type="submit" class="btn btn-primary">Login</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                {{-- RESULT --}}
                                <div id="result" style="display: none;"class="row select result"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <script src="{{ asset('Frontend/css/quiz/quiz.min.js') }}"></script>
@stop