<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="stylesheet"
          href={{asset('https://use.fontawesome.com/releases/v5.3.1/css/all.css')}} integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
          crossorigin="anonymous">
    <!-- Title -->
    <title>PI</title>

    <!-- Favicon -->
    <link rel="icon" href="{{asset('/images/core-img/iEx51.png')}}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('/Frontend/css/style.css')}}">
    <meta name="x-token" content="{{ csrf_token() }}"/>

    @yield('head')

    <style type="text/css">
        h1 {
            margin: 0 0 20px 0;
            font-weight: 300;
            font-size: 28px;
        }

        input[type="text"],
        input[type="password"] {
            display: block;
            box-sizing: border-box;
            margin-bottom: 20px;
            padding: 4px;
            width: 100%;
            height: 32px;
            border: none;
            border-bottom: 1px solid #AAA;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            font-size: 15px;
            transition: 0.2s ease;
        }

        input[type="date"] {
            display: block;
            box-sizing: border-box;
            margin-bottom: 20px;
            padding: 4px;
            width: 220px;
            height: 32px;
            border: none;
            border-bottom: 1px solid #AAA;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            font-size: 15px;
            transition: 0.2s ease;

        }

        input[type="date"]:focus {
            border-bottom: 2px solid #16a085;
            color: #16a085;
            transition: 0.2s ease;
        }

        input[type="text"]:focus,
        input[type="password"]:focus {
            border-bottom: 2px solid #16a085;
            color: #16a085;
            transition: 0.2s ease;
        }

        input[type="submit"] {
            width: 100%;
            height: 32px;
            background: #16a085;
            border: none;
            border-radius: 2px;
            color: #FFF;
            font-family: 'Roboto', sans-serif;
            font-weight: 500;
            text-transform: uppercase;
            transition: 0.1s ease;
            cursor: pointer;
        }

        input[type="submit"]:hover,
        input[type="submit"]:focus {
            opacity: 0.8;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
            transition: 0.1s ease;
        }

        input[type="submit"]:active {
            opacity: 1;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
            transition: 0.1s ease;
        }

        input[type="button"] {
            margin-top: 28px;
            width: 100px;
            height: 32px;
            background: #16a085;
            border: none;
            border-radius: 2px;
            color: #FFF;
            font-family: 'Roboto', sans-serif;
            font-weight: 500;
            text-transform: uppercase;
            transition: 0.1s ease;
            cursor: pointer;
        }

        input[type="button"]:hover,
        input[type="button"]:focus {
            opacity: 0.8;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
            transition: 0.1s ease;
        }

        input[type="button"]:active {
            opacity: 1;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
            transition: 0.1s ease;
        }

        .or {
            position: absolute;
            top: 225px;
            left: 280px;
            width: 40px;
            height: 40px;
            background: #DDD;
            border-radius: 50%;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
            line-height: 40px;
            text-align: center;
        }

        .element {
            display: inline-flex;
        }

        .element .group {
            display: inline-block;
            position: relative;
            cursor: pointer;
            margin: 0px;
        }

        .element button {
            padding: 10px 20px;
            font-size: 16px;
            border-radius: 2px;
            cursor: inherit;
            border: 0;
            color: #ffffff;
            width: 150px;
            outline: 0;
            font-family: inherit;
            -webkit-transition: background-color 0.2s ease-in-out;
            -moz-transition: background-color 0.2s ease-in-out;
            -o-transition: background-color 0.2s ease-in-out;
            transition: background-color 0.2s ease-in-out;
        }

        .fb {
            background-color: #306199;
        }

        .fb:hover {
            background-color: #244872;
        }

        .gp {
            background-color: #e93f2e;
        }

        .gp:hover {
            background-color: #ce2616;
        }
    </style>

</head>

<body>

<!-- ***** Header Area Start ***** -->
@include('Frontend.partial.head')
<!-- ***** Header Area End ***** -->

@if ($message = Session::get('success'))
    <div class="alert-success">
        <div class="modal fade" id="overlay">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>
                            {{ $message }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if ($message = Session::get('warning'))
    <div class=alert-warning">
        <div class="modal fade" id="overlay">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>
                            {{ $message }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if (session('msg'))
    <div class="alert-success">
        <div class="modal fade" id="overlay">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        {{ session('msg') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endif
@if ($errors->any())
    <div class="alert-danger">
        <div class="modal fade" id="overlay">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(Session::has('flash_message_error'))
    <div class="alert-danger">
        <div class="modal fade" id="overlay">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <strong>{!! session('flash_message_error') !!}</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(Session::has('flash_message_success'))
    <div class="lert-success">
        <div class="modal fade" id="overlay">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <strong>{!! session('flash_message_success') !!}</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 340px !important;">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login-page">
                    <div class="form" style="padding: 25px 10px !important;">
                        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        <h1>Log In</h1>
                        <form id="formRegister" class="login-form" method="POST"
                              action="{{ url('/login_quiz') }}">
                            {{csrf_field()}}
                            <input type="text" name="email" id="email"
                                   placeholder="Please Enter your email"/>
                            <input type="password" name="password" id="password"
                                   placeholder="Please Enter your password"/>
                            <button type="submit">login</button>
                            <button type="button"
                                    style="background-color: red;color: white; margin-top: 5px"
                                    data-dismiss="modal">Cancel
                            </button>
                            <p class="message">Not registered?
                                <a href="{{ url('/register')}}" data-toggle="modal"
                                   data-target="#exampleModalRegister">Create an
                                    account</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModalRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true" style="top: 100px;">
    <div class="modal-dialog" role="document" style="max-width: 340px !important;">
        <div class="modal-content">
            <div class="modal-body">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>
                            {{ $message }}
                        </p>
                    </div>
                @endif
                @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <p>
                            {{ $message }}
                        </p>
                    </div>
                @endif
                @if (session('msg'))
                    <div class="alert alert-success">
                        {{ session('msg') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(Session::has('flash_message_error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close"
                                data-dismiss="alert">×
                        </button>
                        <strong>{!! session('flash_message_error') !!}</strong>
                    </div>
                @endif
                @if(Session::has('flash_message_success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close"
                                data-dismiss="alert">×
                        </button>
                        <strong>{!! session('flash_message_success') !!}</strong>
                    </div>
                @endif
                <h1>Sign up</h1>
                <form method="POST" action="{{ url('/register') }}">
                    {{csrf_field()}}
                    <input type="text" name="name" placeholder="Username"/>
                    <select name="gender" value="sex"
                            style="font-size: 15px;border: none;margin-bottom: 5px;height: 33px; color: dimgrey">
                        <option name="male">Male</option>
                        <option name="female">Female</option>
                    </select>


                    <input type="date" class="form-control pull-right" name="dob" id="datepicker">


                    <input type="text" name="email" placeholder="E-mail"/>
                    <input type="password" name="password" placeholder="Password"/>
                    <input type="password" name="password_confirmation" placeholder="Retype password"/>
                    <div class="form-group">
                        <input type="submit" name="signup_submit" value="Sign up"/>
                    </div>
                    <span>Log in with</span>
                    <hr style="margin-top: 5px"/>

                    <div class="element">
                        <div class="group facebook-share" style="margin-right: 5px  ">
                            <button class="fb"><i class="fa fa-facebook"></i> facebook</button>
                        </div>
                        <div class="group gplus-share">
                            <button class="gp"><i class="fa fa-google"></i> google</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Content-->
@yield('content')
{{-- End Content --}}
<!-- ***** Footer Area Start ***** -->

@include('Frontend.partial.footer')
<!-- ***** Footer Area End ***** -->

<!-- ##### All Javascript Files ##### -->
<!-- jQuery-2.2.4 js -->
<script src="{{ asset('Frontend/js/jquery/jquery-2.2.4.min.js') }}"></script>
<!-- Popper js -->
<script src="{{ asset('Frontend/js/bootstrap/popper.min.js') }}"></script>
<!-- Bootstrap js -->
<script src="{{ asset('Frontend/js/bootstrap/bootstrap.min.js') }}"></script>
<!-- All Plugins js -->
<script src="{{ asset('Frontend/js/plugins/plugins.js') }}"></script>
<!-- Active js -->
<script src="{{ asset('Frontend/js/active.js') }}"></script>
<script>
    $('#overlay').modal('show');

    setTimeout(function () {
        $('#overlay').modal('hide');
    }, 3000);
</script>
@yield('script')

</body>

</html>
