<!-- ##### Header Area Start ##### -->

<header class="header-area">

    <!-- Navbar Area -->
    <div class="viral-news-main-menu" id="stickyMenu">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="viralnewsNav">

                    <!-- Logo -->
                    <a class="nav-brand" href="/" style="height: auto;max-width: 10%;">
                        <img src="{{ asset('/images/core-img/iEx51.png') }}" alt="">


                    </a>
                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- close btn -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>
                                <li @yield('home') ><a href="{{asset('/')}}">Home</a></li>
                                <li @yield('list-article') ><a href="{{asset('/list-article')}}">Acticles</a></li>
                                <li @yield('videos')><a href="{{asset('/video')}}">Videos</a></li>
                                <li @yield('quizzes')><a href="{{asset('/quizzes')}}">Quiz</a></li>
                                <li @yield('about')><a href="{{asset('/about')}}">About PI</a>
                                    <div class="megamenu"
                                         style="background-image: url({{ asset('images/blog-img/bg3.jpg') }});">
                                        <ul class="single-mega cn-col-4">
                                            <a class="nav-brand" href="{{asset('/')}}"
                                               style="height: auto;max-width: 100%;"><img
                                                        src="{{asset("/images/blog-img/add.png")}}" alt=""></a>
                                        </ul>
                                        <ul class="single-mega cn-col-4">
                                            <li><a href="{{asset('/')}}">Who We are?</a>
                                                <!-- Single Blog Post -->
                                                <div class="col-12 col-lg-12">
                                                    <div class="single-blog-post style-3">
                                                        <!-- Post Thumb -->
                                                        <div class="post-thumb">
                                                            <!-- Embat video -->
                                                            <div class="embed-responsive embed-responsive-16by9">
                                                                <iframe width="560" height="415"
                                                                        src="https://www.youtube-nocookie.com/embed/Vp0yPAroGV4"
                                                                        frameborder="0"
                                                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                        allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="single-mega cn-col-4">
                                            <li><a href="{{asset('/')}}">What We do?</a></li>
                                            <a class="nav-brand" href="{{asset('/')}}"
                                               style="height: auto;max-width: 100%;"><img
                                                        src="{{asset("/images/core-img/iEx51.png")}}" alt=""></a>
                                        </ul>

                                    </div>
                                </li>
                            </ul>

                            <!-- Search Button -->
                            <div class="search-btn">
                                <i id="searchbtn" class="fa fa-search" aria-hidden="true"></i>
                            </div>

                            <!-- Search Form -->
                            <div class="viral-search-form">
                                <form id="search" action="#" method="get">
                                    <input type="text" name="search-terms" placeholder="Enter your keywords ...">
                                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>

                            <!-- login Button -->
                            @if(Auth::guard('userclients')->guest())
                                <div class="add-post-button">
                                    <button class="btn add-post-btn " type="button"  data-toggle="modal" data-target="#exampleModalCenter">LOGIN</button>
                                </div>
                            @elseif(Auth::guard('userclients')->check())
                                <!--Dropdown -->
                                <div class="dropdown">
                                    <!--Trigger-->
                                    <button class="btn add-post-btn dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::guard('userclients')->user()->name }}</button>
                                    <!--login button-->
                                    <div class="dropdown-menu">
                                        <div class="btn-logout">
                                            <a href="{{ url('/logout') }}">LOGOUT</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            var link = "www.youtube.com";
            $('#shareIcons').jsSocials({
                url: link,
                text: 'This is youtube',
                showLabel: false,
                showCount: "inside",
                shareIn: "popup",
                shares: ["facebook", "twitter", "pinterest", "googleplus"]
            });
        });
    </script>
</header>


<!-- ##### Header Area End ##### -->
