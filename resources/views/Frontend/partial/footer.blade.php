<!-- ##### Footer Area Start ##### -->
<footer class="footer-area" style="padding-top: 0 !important;">
    <!-- Main Footer Area -->
    <div class="main-footer-area">
        <div class="container">
            <div class="row" style="margin-top: 5px;">

                <!-- Footer Widget Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer-widget-area" style="margin-top: 10px">
                        <div class="contact">
                            <h3>
                                Contact Information
                            </h3>
                            <p>
                                <span class="prefix">Phone :</span>
                                +855 78-800-648
                                <br>
                                <span class="prefix">Email :</span>
                                iextysk@gmail.com
                            </p>
                            <h3>
                                Follow Us
                            </h3>
                            <svg class="svg--source" width="0" height="0" aria-hidden="true">

                                <symbol id="svg--facebook" viewbox="0 -7 16 30">
                                    <path d="M12 3.303h-2.285c-0.27 0-0.572 0.355-0.572 0.831v1.65h2.857v2.352h-2.857v7.064h-2.698v-7.063h-2.446v-2.353h2.446v-1.384c0-1.985 1.378-3.6 3.269-3.6h2.286v2.503z" />
                                </symbol>

                                <symbol id="svg--youtube" viewbox="-150 -150 800 800">
                                    <path d="M459,61.2C443.7,56.1,349.35,51,255,51c-94.35,0-188.7,5.1-204,10.2C10.2,73.95,0,163.2,0,255s10.2,181.05,51,193.8
			C66.3,453.9,160.65,459,255,459c94.35,0,188.7-5.1,204-10.2c40.8-12.75,51-102,51-193.8S499.8,73.95,459,61.2z M204,369.75v-229.5
			L357,255L204,369.75z" />
                                </symbol>
                                <symbol id="svg--instagram" viewBox="-10 16 70 15">
                                    <path d="M 16 3 C 8.83 3 3 8.83 3 16 L 3 34 C 3 41.17 8.83 47 16 47 L 34 47 C 41.17 47 47 41.17 47 34 L 47 16 C 47 8.83 41.17 3 34 3 L 16 3 z M 37 11 C 38.1 11 39 11.9 39 13 C 39 14.1 38.1 15 37 15 C 35.9 15 35 14.1 35 13 C 35 11.9 35.9 11 37 11 z M 25 14 C 31.07 14 36 18.93 36 25 C 36 31.07 31.07 36 25 36 C 18.93 36 14 31.07 14 25 C 14 18.93 18.93 14 25 14 z M 25 16 C 20.04 16 16 20.04 16 25 C 16 29.96 20.04 34 25 34 C 29.96 34 34 29.96 34 25 C 34 20.04 29.96 16 25 16 z"></path>
                                </symbol>
                            </svg>

                            <div class="wrapper">
                                <div class="connect">

                                    <a href="" rel="author" class="share facebook">
                                        <svg role="presentation" class="svg--icon">
                                            <use xlink:href="#svg--facebook" />
                                            <span class="clip">FACEBOOK</span>
                                        </svg>
                                    </a>

                                    <a href="" class="share  youtube">
                                        <svg role="presentation" class="svg--icon">
                                            <use xlink:href="#svg--youtube" />
                                            <span class="clip">YOU-TUBE</span>
                                        </svg>
                                    </a>

                                    <a href="" class="share  instagram">
                                        <svg role="presentation" class="svg--icon">
                                            <use xlink:href="#svg--instagram" />
                                            <span class="clip">INSTAGRAM</span>
                                        </svg>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <div align="center" class="" style="text-align: center;margin-top: 10px">
                        <div class="fb-page"
                             data-href="https://www.facebook.com/iextysk/"
                             data-width="380"
                             data-hide-cover="false"
                             data-show-facepile="true"></div>
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=110121469672979&autoLogAppEvents=1';
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));
                        </script>
                    </div>
                </div>

                @yield('link_language')


            </div>
        </div>
    </div>
    <!-- Bottom Footer Area -->
    <div class="bottom-footer-area">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Copywrite -->
                    <p>
                        <a href="#">Copyright &copy;
                            <script>document.write(new Date().getFullYear());</script>
                            All rights reserved
                            <i class="" aria-hidden="true"></i> by
                            <a href="#" target="_blank">Developers</a>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- ##### Footer Area Start ##### -->
