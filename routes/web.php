<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

Route::get('/quizzes', 'QuizController@index');
Route::get('/', 'HomeController@index');
//Route::get('/','QuizzesController@show');
Route::get('/language/{lang_id}', 'HomeController@showWithLanguage')->name('home.language');

Route::get('/list-article', 'ArticleController@index')->name('article.index');
Route::get('/list-article/language/{lang_id}', 'ArticleController@showWithLanguage')->name('article.language');
Route::get('/list-article-category/{id}', 'ArticleController@showArtWithCategory')->name('article.category');

Route::get('/video', 'VideoController@index')->name('video');
Route::get('/video-detail/{id}', 'VideoController@show')->name('video.show');

Route::get('/article-detail/{id}', 'ArticleDetailController@index')->name('article_detail.index');
Route::get('/articel_detail/{id}/{lang_id}', 'ArticleDetailController@translateArticle')->name('article_detail.translate');

Route::get('/about', 'AboutController@index')->name('about');

#New
Route::get('/quiz-detail/{id}', 'QuizController@show')->name('quiz.show');

Route::post('/answer', 'QuizController@answer');

Route::group([], function () {
    Route::get('/admin', 'AdminController@admin')->name('admin');
    Route::get('/admin/draftarticle', 'adminController@Draftarticle');
    Route::get('/admin/list-article', 'AdminController@ListArticle')->name('admin.list-article');
    Route::get('/admin/admin', 'adminController@ListAdmin');
    Route::get('/admin/add-new-admin', 'adminController@AddAdmin');
    Route::get('/admin/banned', 'adminController@banned');

    Route::get('/admin/all-user', 'adminController@alluser');

    Route::get('/admin/all-quizzes', 'adminController@allquizzes');
    Route::get('/admin/draft-quiz', 'adminController@Draftquize');
    Route::get('/admin/add-new-quiz', 'adminController@addnewquiz');
    Route::get('/admin/allvideos', 'adminController@allvideos');
    Route::get('/admin/draftvideo', 'adminController@draftvideo');
    Route::get('/admin/addnewvideo', 'adminController@addnewvideo');
    Route::get('/admin/addnewarticle', 'adminController@addnewarticle');
    Route::get('/register', 'adminController@registerform');
    Route::get('/login', 'adminController@login');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/language', function (\Illuminate\Http\Request $request) {
    //    dd($request->all());
    $languagefilter = DB::table('posts')->where('is_feature', 1)->where('category_id', $request->category_id)->where('language_id', '!=', $request->language_id)->get();

    return $languagefilter;
})->middleware('auth');

Route::post('/insert-tag', function (\Illuminate\Http\Request $request) {
    $lastKey = count($request->tagsArray) - 1;
    $lastKeyValue = $request->tagsArray[$lastKey];

    $hasData = App\Posttag::where('post_id', '=', 0)->count();

    if ($hasData > 0) {
        DB::table('posttags')->where('post_id', '=', 0)->delete();
        for ($i = 0; $i < count($lastKeyValue); $i++) {
            $posttag = new App\Posttag;
            $posttag->tag_id = $lastKeyValue[$i];
            $posttag->post_id = 0;
            $posttag->save();
        }
    } else {
        for ($i = 0; $i < count($lastKeyValue); $i++) {
            $posttag = new App\Posttag;
            $posttag->tag_id = $lastKeyValue[$i];
            $posttag->post_id = 0;
            $posttag->save();
        }
    }
})->middleware('auth');

//kemhong
Auth::routes();
Route::get('/userclient/activation/{token}', 'Auth\RegisterController@userclientsActivation')->name('register');
Route::match(['get', 'post'], '/login', 'LoginClientsController@client_login');
Route::match(['get', 'post'], '/login_quiz', 'LoginClientsController@client_login_2')->name('login.quiz');
Route::get('logout', 'LoginClientsController@logout');

Route::post('/answer_add', 'Answer_addController@index');
Route::post('/', 'QuizController@insert_answer_user');


Route::post('/user_result', 'QuizController@answer');


