<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserclientsActivationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userclients_activations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_userclient')->unsigned();
            $table->foreign('id_userclient')->references('id')->on('userclients')->onDelete('cascade');
            $table->string('token');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("userclients_activations");
        Schema::table('userclients', function (Blueprint $table) {
            $table->dropColumn('is_activated');
        });
    }
}
