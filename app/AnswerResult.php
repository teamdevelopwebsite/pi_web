<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerResult extends Model
{
   protected $table="answer_results";
}
