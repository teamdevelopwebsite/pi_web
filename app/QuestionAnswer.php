<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
   protected $table = "v_answer_result";

   public $timestamps = false;
}
