<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz_user extends Model
{
    protected $table='quiz_users';

    protected $fillable = [
        'userclient_id', 'quiz_id', 'take_date',
    ];
}
