<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Userclient extends Authenticatable
{
    use Notifiable;
    protected $guard = 'customers';

    protected $fillable = [
        'name', 'gender', 'dob', 'age', 'email', 'elid', 'password', 'remember_token', 'profileImage', 'is_activated', 'created_at','updated_at', 'is_activated',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
