<?php

namespace App\Http\Controllers;

use App\AnswerUser;
use App\Quiz_user;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Answer_addController extends Controller
{
    protected function index(Request $request)
    {
        $client_id= Auth::guard('userclients')->user()->id ;
        $mytime = Carbon::now();

        $count=count($request->data);
        for($i=0;$i<$count;$i++)
        {
            $objModel = new AnswerUser();
            $objModel->userclient_id = $client_id;
            $objModel->ans_id = $request->data[$i]["answerpk"];
            $objModel->save();
        }
        $json=($request->data[0]["answer"]);
        $array = json_decode( $json, true );
        return Quiz_user::create([
            'userclient_id' => $client_id,
            'quiz_id' => 10,
            'take_date' => $mytime,
        ]);
    }
}
