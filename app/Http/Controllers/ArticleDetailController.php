<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $art = DB::table('allarticles')->where('id', '=', $id)->get();
        $topArt = DB::table('toparticles')->get();
        $arrIndex = [];
        for ($i = 1; $i <= count($topArt); $i++) {
            array_push($arrIndex, $i);
        }
        $i = 0;
        foreach ($topArt as $ta) {
            $ta->index = $arrIndex[$i];
            $i++;
        }

        // Get Tags
        $getTags = DB::table('posttags')->select('tag_id')->where('post_id', '=', $id)->get();
        $arrTags = [];
        foreach ($getTags as $arr) {
            array_push($arrTags, $arr->tag_id);
        }
        $relateTag = DB::table('relatetags')->whereIn('tid', $arrTags)->where('id', '!=', $id)->get();

        // Get comment
        $comment = DB::table('getcommentposts')->where('post_id', '=', $id)->get();

        // Get language
        $language = DB::table('postlanguages')->distinct()->where('link_to', '!=', null)->get();

        return view('Frontend.pages.article_detail', ['art' => $art, 'topArt' => $topArt, 'relateTag' => $relateTag, 'comment' => $comment, 'language' => $language]);
    }

    public function translateArticle($article_id, $lang_id)
    { }
}
