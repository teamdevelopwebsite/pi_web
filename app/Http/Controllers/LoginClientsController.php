<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use Illuminate\Support\Collection;

class LoginClientsController extends Controller
{
    public function client_login(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::guard('userclients')->attempt(['email' => $data['email'], 'password' => $data['password'], 'is_activated' => '1'])) {
                return redirect('/')->with('flash_message_success', 'Customer Login');
            } elseif (Auth::guard('userclients')->attempt(['email' => $data['email'], 'password' => $data['password'], 'is_activated' => '0'])) {
                Auth::guard('userclients')->logout();
                return redirect('/login')->with('flash_message_error', 'Customer not yet verify');
            } else {
                Auth::guard('userclients')->logout();
                return redirect('/login')->with('flash_message_error', 'Customer Login False');
            }
        }
        return view('Frontend.pages.login');
    }

//    Log In
    public function client_login_2(Request $request)
    {
//        dd($request->all());

        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::guard('userclients')->attempt(['email' => $data['email'], 'password' => $data['password'], 'is_activated' => '1'])) {
                // Successfully logi
                return redirect()->back()->with('flash_message_success', 'Login Success');
            } elseif (Auth::guard('userclients')->attempt(['email' => $data['email'], 'password' => $data['password'], 'is_activated' => '0'])) {
                Auth::guard('userclients')->logout();
                return redirect()->back()->with('flash_message_error', 'Customer not yet verify');
            } else {
                Auth::guard('userclients')->logout();
                return redirect()->back()->with('flash_message_error', 'Customer Login False');
            }
        }
        return view('Frontend.pages.login_2');
    }

    public function logout()
    {
        Auth::guard('userclients')->logout();
//        Session::flush();
//        dd(Auth::check());
//        Session::guard('userclients')->flush();
        return redirect()->back()->with('flash_message_success', 'logout success');
//        return redirect('/')->back('flash_message_success','logout success');
    }


}
