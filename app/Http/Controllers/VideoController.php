<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Feature Video
        $featureVid = DB::table('allvideos')->where('is_feature', '=', 1)->inRandomOrder()->limit(1)->get();
        if (count($featureVid) == 0) {
            $featureVid = DB::table('allvideos')->inRandomOrder()->limit(1)->get();
        }

        $playList = DB::table('allvideos')->inRandomOrder()->get();

        // Top video
        $topVid = DB::table('topvideos')->limit(10)->get();
        $arrIndex = [];
        for ($i=1;$i<=count($topVid);$i++) {
            array_push($arrIndex, $i);
        }
        $i = 0;
        foreach ($topVid as $ta) {
            $ta->index = $arrIndex[$i];
            $i++;
        }

        // Get Tags
        $getTags = DB::table('videotags')->select('tag_id')->where('vid_id', '=', $featureVid[0]->id)->get();
        $arrTags = [];
        foreach ($getTags as $arr) {
            array_push($arrTags, $arr->tag_id);
        }
        $relateVidTag = DB::table('relatevideotags')->whereIn('tid', $arrTags)->limit(8)->get();

        // Get language
        $language = DB::table('languages')->get();

        return view('Frontend.pages.list-video', ['featureVid' => $featureVid, 'playList' => $playList, 'topVid' => $topVid, 'relateVidTag'=>$relateVidTag, 'language'=>$language]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Show video
        $showVid = DB::table('allvideos')->where('id', '=', $id)->get();

        // Top video
        $topVid = DB::table('topvideos')->limit(8)->get();
        $arrIndex = [];
        for ($i=1;$i<=count($topVid);$i++) {
            array_push($arrIndex, $i);
        }
        $i = 0;
        foreach ($topVid as $ta) {
            $ta->index = $arrIndex[$i];
            $i++;
        }

        // Get Tags
        $getTags = DB::table('videotags')->select('tag_id')->where('vid_id', '=', $showVid[0]->id)->get();
        $arrTags = [];
        foreach ($getTags as $arr) {
            array_push($arrTags, $arr->tag_id);
        }
        $relateVidTag = DB::table('relatevideotags')->whereIn('tid', $arrTags)->limit(8)->get();


        // Get language
        $language = DB::table('languages')->get();

        return view('Frontend.pages.video-detail', ['showVid'=>$showVid, 'relateVidTag'=>$relateVidTag, 'topVid'=>$topVid, 'language' => $language]);
    }
}
