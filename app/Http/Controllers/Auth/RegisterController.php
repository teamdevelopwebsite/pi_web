<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Userclient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Mail;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/Frontend.Pages.register-form';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:50',
            'email' => 'required|email|max:255|unique:userclients',
            'password' => 'required|min:6|confirmed',
            'gender' => 'required',
            'dob' => 'required|',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return Userclient
     */
    protected function create(array $data)
    {
        $myDate = $data['dob'];
        $years = Carbon::parse($myDate)->age;
        return Userclient::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'gender' => $data['gender'],
            'password' => bcrypt($data['password']),
            'dob' => $data['dob'],
            'age' => $years,
            'is_activated' => 0
        ]);
    }

    public function register(Request $request)
    {
        $input = $request->all();
        $validator = $this->validator($input);

        if ($validator->passes()) {
            $userclient = $this->create($input)->toArray();
//            $userclient['link'] = str_random(30);
            $userclient['link'] = mt_rand(000001, 999999);

            DB::table('userclients_activations')->insert(['id_userclient' => $userclient['id'], 'token' => $userclient['link']]);

            Mail::send('emails.activation', $userclient, function ($message) use ($userclient) {
                $message->to($userclient['email']);
                $message->subject('Activation Code');
            });
            return redirect()->back()->with('success', "We sent activation code. Please check your mail.");
        }
        return back()->with('errors', $validator->errors());
    }

    public function userclientsActivation($token)
    {
        $check = DB::table('userclients_activations')->where('token', $token)->first();
        if (!is_null($check)) {
            $userclients = Userclient::find($check->id_userclient);
            if ($userclients->is_activated == 1) {
                return redirect()->to('login')->with('success', "You are already actived.");
            }
            $userclients->update(['is_activated' => 1]);
            DB::table('userclients_activations')->where('token', $token)->delete();
            return redirect()->to('/')->with('success', "You active successfully.");
        }
        return redirect()->to('login')->with('Warning', "your token is invalid");
    }
}
