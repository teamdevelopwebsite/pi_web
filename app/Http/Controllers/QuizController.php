<?php

namespace App\Http\Controllers;

use App\Quiz;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\DB;

class QuizController extends Controller
{
    public function index()
    {
        $quizs = DB::table('v_user_create_quizs')->paginate(12);;
        $quizs_feature = DB::table('v_user_create_quizs')
            ->latest()
            ->first();

        return view('Frontend/pages/quizzes', compact('quizs', 'quizs_feature'));
    }

    //Create Quiz

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = 10)
    {
        $quize = DB::table('quizs')->where('id', '=', $id)->get();

        $user = DB::table('users')->where('id', '=', $quize[0]->user_id)->get();

        $quiz_detail = DB::table('quiz_details')->where('quiz_id', '=', $id)->get();

        $answer = DB::table('answers')->get();

        return view('Frontend.pages.quiz_view', ['quizs' => $quize, 'quiz_detail' => $quiz_detail, 'answer' => $answer, 'user' => $user]);
    }

    public function answer(Request $request)
    {
        $number_of_questions = (count($request->data));
        $user_answers = array();
        $answers_id = array();
        for ($i = 0; $i < $number_of_questions; $i++) {
            array_push($user_answers, ($request->data[$i]['answer']));
            array_push($answers_id, ($request->data[$i]['id']));
        }
        $answer_id = ($request->data[0]['answer']);
        $each_result = DB::table('v_answer_result')->whereIn('answer_id', $user_answers)->where('answer_id', $answer_id)->get(['result_id', 'result', 'percentage', 'result_image'])->toArray();

        $main_array = array();
        for ($i = 0; $i < count($each_result); $i++) {
            $percentage = $each_result[$i]->result;
            $id = $each_result[$i]->result_id;
            $result_img = $each_result[$i]->result_image;
            $results_show = DB::table('v_answer_result')->whereIn('answer_id', $user_answers)->where('result_id', $id)->sum('percentage');
            array_push($main_array, array(
                "percentage" => $results_show,
                "result" => $percentage,
                "result_image" => $result_img,
            ));
        }
        $main_array = max($main_array);

        // return $main_array;

        return Response::json(array(
            'success' => true,
            'data'   => $main_array
        ));
    }
}
