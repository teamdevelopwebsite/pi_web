<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function admin(){
        return view('Backend.main');
    }
    public function ListArticle(){
        return view('Backend.pages.list_article');
    }
    public function ListAdmin(){
        return view('Backend.pages.admin');
    }
    public function AddAdmin(){
        return view('Backend.pages.addnewadmin');
    }
    public function AllUser(){
        return view('Backend.pages.alluser');
    }
    public function Allquizzes(){
        return view('Backend.pages.all_quizzes');
    }
    public function Draftquize(){
        return view('Backend.pages.draft_quize');
    }
    public function Addnewquiz(){
        return view('Backend.pages.addnewquiz');
    }
    public function Draftarticle(){
        return view('Backend.pages.draftarticle');
    }
    public function Allvideos(){
        return view('Backend.pages.allvideos');
    }
    public function Draftvideo(){
        return view('Backend.pages.draftvideo');
    }
    public function Addnewvideo(){
        return view('Backend.pages.addnewvideo');
    }
    public function Addnewarticle(){
        return view('Backend.pages.addnewarticle');
    }
    public function Registerform(){
        return view('Frontend.pages.register-form');
    }
    public function Login(){
        return view('Frontend.pages.login');
    }
}
