<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topVid = DB::table('topvideos')->limit(8)->get();
        $topArt = DB::table('toparticles')->limit(10)->get();

        $allArt = DB::table('allarticles')->limit(12)->get();
        $latestArt = DB::table('allarticles')->orderBy('post_date', 'asc')->limit(3)->get();

        $allVidRand = DB::table('allvideos')->inRandomOrder()->limit(10)->get();
        $allArtRand = DB::table('toparticles')->inRandomOrder()->limit(10)->get();

        $featureArt = DB::table('allarticles')->where('is_feature', '=', 1)->get();

        $arrIndex = [];
        for ($i=1;$i<=count($topArt);$i++) {
            array_push($arrIndex, $i);
        }
        $i = 0;
        foreach ($topArt as $ta) {
            $ta->index = $arrIndex[$i];
            $i++;
        }

        // Get language
        $language = DB::table('languages')->get();

        return view('Frontend.pages.homepage', ['topVid' => $topVid, 'topArt' => $topArt, 'allArt' => $allArt, 'latestArt' => $latestArt, 'allVidRand' => $allVidRand, 'allArtRand' => $allArtRand, 'featureArt' => $featureArt, 'language' => $language]);
    }

    public function showWithLanguage($id) {
        $topVid = DB::table('topvideos')->limit(8)->get();
        $topArt = DB::table('toparticles')->where('lang_id','=',$id)->limit(10)->get();

        $allArt = DB::table('allarticles')->where('lang_id','=',$id)->limit(12)->get();
        $latestArt = DB::table('allarticles')->where('lang_id','=',$id)->orderBy('post_date', 'asc')->limit(3)->get();

        $allVidRand = DB::table('allvideos')->inRandomOrder()->limit(10)->get();
        $allArtRand = DB::table('toparticles')->where('lang_id','=',$id)->inRandomOrder()->limit(10)->get();

        $featureArt = DB::table('allarticles')->where('lang_id','=',$id)->where('is_feature', '=', 1)->get();

        $arrIndex = [];
        for ($i=1;$i<=count($topArt);$i++) {
            array_push($arrIndex, $i);
        }
        $i = 0;
        foreach ($topArt as $ta) {
            $ta->index = $arrIndex[$i];
            $i++;
        }

        // Get language
        $language = DB::table('languages')->get();

        return view('Frontend.pages.homepage', ['topVid' => $topVid, 'topArt' => $topArt, 'allArt' => $allArt, 'latestArt' => $latestArt, 'allVidRand' => $allVidRand, 'allArtRand' => $allArtRand, 'featureArt' => $featureArt, 'language' => $language]);

    }

}
