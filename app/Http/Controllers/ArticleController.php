<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topArt = DB::table('toparticles')->get();
        $arrIndex = [];
        for ($i=1;$i<=count($topArt);$i++) {
            array_push($arrIndex, $i);
        }
        $i = 0;
        foreach ($topArt as $ta) {
            $ta->index = $arrIndex[$i];
            $i++;
        }

        $allArt = DB::table('allarticles')->get();

        $catFeature = DB::table('allarticles')->where('cat_feature', '=', 1)->inRandomOrder()->limit(1)->get();
        if ($catFeature->count() == 0) {
            $catFeature = DB::table('allarticles')->inRandomOrder()->limit(1)->get();
        }

        // Get language
        $language = DB::table('postlanguages')->select(['language', 'language_id'])->distinct()->get();

        return view('Frontend/pages/list-article', ['allArt' => $allArt, 'topArt' => $topArt, 'catFeature' => $catFeature, 'language' => $language]);
    }

    public function showWithLanguage($id)
    {
        $topArt = DB::table('toparticles')->where('lang_id','=',$id)->get();
        $arrIndex = [];
        for ($i=1;$i<=count($topArt);$i++) {
            array_push($arrIndex, $i);
        }
        $i = 0;
        foreach ($topArt as $ta) {
            $ta->index = $arrIndex[$i];
            $i++;
        }

        $allArt = DB::table('allarticles')->where('lang_id','=',$id)->get();

        $catFeature = DB::table('allarticles')->where('cat_feature', '=', 1)->where('lang_id', '=', $id)->inRandomOrder()->limit(1)->get();
        if ($catFeature->count() == 0) {
            $catFeature = DB::table('allarticles')->where('lang_id','=',$id)->inRandomOrder()->limit(1)->get();
        }

        // Get language
        $language = DB::table('postlanguages')->select(['language', 'language_id'])->distinct()->get();

        return view('Frontend/pages/list-article', ['allArt' => $allArt, 'topArt' => $topArt, 'catFeature' => $catFeature, 'language' => $language]);
    }

    public function showArtWithCategory($id) {
        $topArt = DB::table('toparticles')->get();
        $allArt = DB::table('allarticles')->where('cat_id', '=', $id)->get();

        $catFeature = DB::table('allarticles')->where('cat_feature', '=', 1)->limit(1)->get();

        return view('Frontend/pages/list-article', ['topArt' => $topArt, 'allArt' => $allArt, 'catFeature' => $catFeature]);

    }

}
