<?php

namespace App\Http\Controllers\Functions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FunctionController extends Controller
{
    public static function generateTime($date_diff) {

        $date = $date_diff < 24 ? (int)$date_diff : (int)($date_diff / 24) < 30 ? (int)($date_diff / 24) : (int)($date_diff / (24*30)) < 12 ? (int)($date_diff / (24*30)) : (int)($date_diff / (24*30*12));
        $ago = $date_diff < 24 ? "hour(s)" : (int)($date_diff / 24) < 30 ? "day(s)" : (int)($date_diff / (24*30)) < 12 ? "month(s)" : "year(s)";

        $str = (string)$date." ".$ago;

        return $str;

    }
}
