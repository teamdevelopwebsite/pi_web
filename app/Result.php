<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    public function question()
    {
        return $this->hasMany('App\QuizDetail');
    }
}
